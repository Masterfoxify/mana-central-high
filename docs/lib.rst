lib package
===========

Submodules
----------

lib.botlogger module
--------------------

.. automodule:: lib.botlogger
    :members:
    :undoc-members:
    :show-inheritance:

lib.diceroller module
---------------------

.. automodule:: lib.diceroller
    :members:
    :undoc-members:
    :show-inheritance:

lib.effectmaker module
---------------------

.. automodule:: lib.effectmaker
    :members:
    :undoc-members:
    :show-inheritance:

lib.grid module
---------------

.. automodule:: lib.grid
    :members:
    :undoc-members:
    :show-inheritance:

lib.players module
------------------

.. automodule:: lib.players
    :members:
    :undoc-members:
    :show-inheritance:

lib.sheets module
---------------------

.. automodule:: lib.sheets
    :members:
    :undoc-members:
    :show-inheritance:

Module contents
---------------

.. automodule:: lib
    :members:
    :undoc-members:
    :show-inheritance:
