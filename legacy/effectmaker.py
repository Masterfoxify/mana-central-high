from math import ceil
import re

class TargetError(Exception):
	pass

class ParseError(Exception):
	pass

class PPTooSmallError(Exception):
	pass

class IFOvermatchError(Exception):
	pass

class TextOverflowError(Exception):
	pass

def walk(arg):
	"""Walks forward a single argument"""
	parse([arg])

def parse(*args):
	"""
	The main parser for 
	"""

	mutable_stats = ('AC', 'MS', 'PHY', 'MAG', 'DETECTION', 'CONTROL')
	immutable_stats = ('AURA', 'HEALTH', 'RESERVE', 'RECOVERY', 'INVEST', 'MOVEMENT', 'BODY', 'AURA', 'TRANSFORMATION', 'DESTRUCTION', 'CREATION', 'LIFE', 'LEVEL')

	if len(args) > 100:
		raise TextOverflowError('Too many tokens.')

	args = [re.sub(r'[^a-z A-Z0-9()*+\-/%_]+', '', x.upper()) for x in args]

	output_code = ''
	i = -1
	pp = 20
	tab_level = 0
	tabs = lambda: '\t' * tab_level

	if re.search(r'(IF SWITCH)|(IF SWITCHB)+').endpos > 0:
		raise IFOvermatchError('Too many IF SWITCH statements.')

	while i < len(args):
		i += 1

		if arg[i+1] in ['+', '-', '/', '*', '**']:
			output_code += f'{walk(arg[i])}{arg[i+1]}{walk(arg[i+2])}'
			i += 2
			continue

		if arg[i] == 'IF':
			output_code += f'{tabs()}if {walk(args[i+1])}:\n'
			tab_level += 1
			i += 1
			pp += 4
			continue

		elif arg[i] == 'ELIF':
			output_code += f'{tabs()}elif {args[i+1]}:\n'
			tab_level += 1
			i += 1
			pp += 2
			continue

		elif arg[i] == 'ELSE':
			output_code += f'{tabs()}else:\n'
			tab_level += 1
			i += 1
			continue

		elif arg[i] == 'END':
			tab_level -= 1
			continue

		elif arg[i] == 'TARGET':
			i += 1

			if arg[i] == 'SELF':
				output_code += f'{tabs()}ab_dict[\'target\'] = \'self\'\n'
				pp -= 2
				continue

			elif arg[i] == 'SINGLE':
				output_code += f'{tabs()}ab_dict[\'target\'] = \'single\'\n'
				pp -= 4
				continue

			elif arg[i] == 'DIRECTION':
				output_code += f'{tabs()}ab_dict[\'target\'] = \'direction\'\n'
				pp -= 10
				continue

			elif arg[i] == 'LOCATION':
				output_code += f'{tabs()}ab_dict[\'target\'] = \'location\'\n'
				pp -= 8
				continue

			else:
				raise TargetError('Incorrect target was chosen')

		elif arg[i] == 'DAMAGE':
			output_temp += f'{tabs()}ab_dict[\'type\'] = \'damage\'\n{tabs()}ab_dict[\'amount\'] = '
			pp -= 6

			i += 1

			if arg[i] in ['AURA', 'BODY', 'TRANSFORMATION', 'CREATION', 'LIFE', 'DESTRUCTION']:
				output_code = output_temp + arg[i].lower() + '\n'
				continue

			elif arg[i+1] == 'PUMPED':
				i += 2

				if arg[i] == 'MANA':
					f'{tabs()}ab_dict[\'pump\'] = \'mana\'\n'
					continue

				elif arg[i] == 'HEALTH':
					f'{tabs()}ab_dict[\'pump\'] = \'health\'\n'
					pp += 6
					continue

				elif arg[i] == 'AURA':
					f'{tabs()}ab_dict[\'pump\'] = \'aura\'\n'
					pp += 4
					continue

			continue

		elif arg[i] == 'HEAL':
			output_temp += f'{tabs()}ab_dict[\'type\'] = \'heal\'\n{tabs()}ab_dict[\'amount\'] = '
			pp -= 4

			i += 1

			if arg[i] in ['AURA', 'BODY', 'TRANSFORMATION', 'CREATION', 'LIFE', 'DESTRUCTION']:
				output_code = output_temp + arg[i].lower() + '\n'
				continue

			elif arg[i+1] == 'PUMPED':
				i += 2

				if arg[i] == 'MANA':
					f'{tabs()}ab_dict[\'pump\'] = \'mana\'\n'
					continue

				elif arg[i] == 'HEALTH':
					f'{tabs()}ab_dict[\'pump\'] = \'health\'\n'
					pp += 8
					continue

				elif arg[i] == 'AURA':
					f'{tabs()}ab_dict[\'pump\'] = \'aura\'\n'
					pp += 6
					continue

			continue

		elif arg[i] == 'LEECH':
			i += 1

			try:
				output_code += f'{tabs()}ab_dict[\'leech\'] = ' + str(int(arg[i])) + '\n'
				pp -= int(ceil(arg[i] / 5))

			except:
				raise ParseError(f'{i}_{arg[i]}')

			continue

		elif arg[i] == 'AOE':
			i += 1

			try:
				output_code += f'{tabs()}ab_dict[\'aoe\'] = ' + str(int(arg[i])) + '\n'
				pp -= int(arg[i] * 2)

			except:
				raise ParseError(f'{i}_{arg[i]}')

			continue

		elif arg[i] == 'MODIFIER':
			i += 1

			try:
				output_code += f'{tabs()}ab_dict[\'mod\'] = ' + str(int(arg[i])) + '\n'
				pp -= int(ceil(arg[i] / 10))

			except:
				raise ParseError(f'{i}_{arg[i]}')

			continue

		elif arg[i] == 'PIERCE':
			i += 1

			try:
				output_code += f'{tabs()}ab_dict[\'pierce\'] = ' + str(int(arg[i])) + '\n'
				pp -= int(arg[i] * 1.5)

			except:
				raise ParseError(f'{i}_{arg[i]}')

			continue

		elif arg[i] == 'SWEEP':
			i += 1

			try:
				output_code += f'{tabs()}ab_dict[\'sweep\'] = ' + str(int(arg[i])) + '\n'
				pp -= int(arg[i] * 1.5)

			except:
				raise ParseError(f'{i}_{arg[i]}')

			continue

		elif arg[i] == 'ADDSTACK':
			i += 1

			output_code += f'{tabs()}ab_dict[\'stacks\'] = \{{arg[i]}: \{\}\}\n'
			continue

		elif arg[i] == 'STACK':
			i += 1
			pp -= 2

			output_code += f'{tabs()}if {arg[i+1]} in ab_dict[\'stacks\'][{arg[i]}]:\n\t{tabs()}ab_dict[\'stacks\'][{arg[i]}][{arg[i+1]}] += 1\n{tabs()}else: ab_dict[\'stacks\'][{arg[i]}][{arg[i+1]}] = 1'

		elif arg[i] == 'UNSTACK':
			i += 1
			pp += 2

			output_code += f'{tabs()}if {arg[i+1]} in ab_dict[\'stacks\'][{arg[i]}]:\n\t{tabs()}if ab_dict[\'stacks\'][{arg[i]}][{arg[i+1]}] != 0: ab_dict[\'stacks\'][{arg[i]}][{arg[i+1]}] -= 1\n'
			continue

		elif arg[i] == 'GETSTACK':
			i += 1

			output_code += f'({arg[i+1]} in ab_dict[\'stacks\'][{arg[i]}] and ab_dict[\'stacks\'][{arg[i]}][{arg[i+1]}] or 0)'
			i += 1
			continue

		elif arg[i] == 'GETALLSTACK':
			i += 1

			output_code += f'ab_dict[\'stacks\'][{arg[i]}]'
			continue

		elif arg[i] == 'SELF':
			i += 1

			if arg[i] == 'STACK':
				i += 1
				output_code += f'ab_dict[\'stack\'][{arg[i]}][\'SELF\']'

			elif arg[i] in mutable_stats or arg[i] in immutable_stats:
				output_code += arg[i].lower()

			continue

	if pp < 0:
		# Yes, this was intentional
		raise PPTooSmallError('PP too small.')