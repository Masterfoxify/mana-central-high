# Discord imports
import discord
from discord.ext import commands

# Built-in imports
from sys import path

path.append('../')

# In-house imports
import main
from lib import botlogger as log
import config as conf

# Dependency imports
from sqlite3 import connect

logging_ranks = log.Logger(log.Configuration('ranks'))

class Ranks:
	def __init__(self, bot):
		self._bot = bot

	@commands.group()
	async def rank(self, ctx):
		"""- Rank command group. Do !help rank for commands"""
		pass

	@rank.command()
	async def get(self, ctx):
		"""- Gets a user's level"""

		if ctx.message.mentions != []:
			user = ctx.message.mentions[0]

		else:
			user = ctx.message.author

		connection = connect('userdat.db')
		cursor = connection.cursor()

		cursor.execute('''SELECT RANKEXP FROM userdat WHERE USERID=? LIMIT 1''', (str(user.id),))
		exp = cursor.fetchone()

		if exp is not None and exp != 0:
			exp = exp[0]
			embed = conf.level_embed(exp)
			embed.set_thumbnail(url=user.avatar_url)
			embed.set_author(name=user.name)

			await ctx.send(embed=embed)

		elif user.bot:
			await ctx.send(f'**{user.name}** is a bot.')

		else:
			await ctx.send(f'**{user.name}** has no experience.')

		logging_ranks.verbose(f'{user.name}\'s level fetched by {ctx.message.author.name}.')

def setup(bot):
	bot.add_cog(Ranks(bot))