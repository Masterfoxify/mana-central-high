import builtins
from psycopg2 import connect

from sys import path
path.append('../')

import config as conf

def init_user(userid, email):
	"""
	Initializes a user in the database and also creates a new sheet for them.

	:param: userid: The user's unique Discord ID
	:param: email: The user's email
	"""

	connection = connect('dbname=userdat.db user=postgres')
	cursor = connection.cursor()

	cursor.execute('INSERT INTO playerdat (USERID, EMAIL) VALUES (%s,%s);', (userid, email))

	connection.commit()

	character_sheet = builtins.gogbot.copy(conf.MAIN_SHEET, title='MCH Character Sheet')

	cursor.execute('UPDATE playerdat SET SHEETID=%s WHERE USERID=%s LIMIT 1;', (character_sheet.id, userid))
	connection.commit()

	character_sheet.share(email, perm_type='user', role='writer')

	cursor.close()
	connection.close()


def load_data(character_sheet, userid):
	"""
	Loads a user's data into a character sheet

	:param: character_sheet: Sheet to load data into
	"""
	
	connection = connect('dbname=userdat.db user=postgres')
	cursor = connection.cursor()

	cursor.execute('SELECT * FROM playerdat WHERE USERID=%s LIMIT 1;', (userid,))

	player_data = cursor.fetchone()

	cursor.close()
	connection.close()


	ws = character_sheet.worksheet('Character Description')

	cells = [
			ws.acell(conf.FULL_NAME),
			ws.acell(conf.EXP),
			ws.acell(conf.RANK),

			ws.acell(conf.AGE),
			ws.acell(conf.BIRTHDAY),
			ws.acell(conf.HEIGHT),
			ws.acell(conf.YEAR),
			ws.acell(conf.GENDER),
			ws.acell(conf.AURA),

			ws.acell(conf.IMAGE1),
			ws.acell(conf.IMAGE2),
			ws.acell(conf.THEME),

			ws.acell(conf.INFO),
			ws.acell(conf.BACKSTORY)
	]

	data_iter = 3

	for cell in cells:
		cell.value = player_data[data_iter]
		data_iter += 1

	ws.update_cells(cells)

	ws = character_sheet.worksheet('Statistics')

	cells = [
			ws.acell(conf.PROTECTOR),
			ws.acell(conf.MAGE),
			ws.acell(conf.WARRIOR),
			ws.acell(conf.CLERIC),
			ws.acell(conf.HERO),

			ws.acell(conf.TITAN),
			ws.acell(conf.PALADIN),
			ws.acell(conf.BERSERK),
			ws.acell(conf.WIZARD),
			ws.acell(conf.SAGACIOUS),
			ws.acell(conf.SORCERER),
			ws.acell(conf.KNIGHT),
			ws.acell(conf.DEFENDER),
			ws.acell(conf.MERCENARY),
			ws.acell(conf.PRIEST),
			ws.acell(conf.ASSASSIN),
			ws.acell(conf.BANDIT),

			ws.acell(conf.STR),
			ws.acell(conf.DEX),
			ws.acell(conf.DUR),
			ws.acell(conf.INTEL),
			ws.acell(conf.CHA),
			ws.acell(conf.WIS),
			ws.acell(conf.PROT),
			ws.acell(conf.MANA),

			ws.acell(conf.DAMAGE),
			ws.acell(conf.MANA_USED),
			ws.acell(conf.TRUE_DMG)
	]

	for cell in cells:
		cell.value = player_data[data_iter]
		data_iter += 1

	ws.update_cells(cells)
	ws.update_title(f'MCH {player_data[3]} Character Sheet')


def save_data(character_sheet, userid):
	"""
	Loads a player's skill data from a sheet into the database. Vastly preferred over :meth:`players.Player.db_sheet_update()`.

	:param: character_sheet: Sheet to save data from
	"""

	connection = connect('dbname=userdat.db user=postgres')
	cursor = connection.cursor()

	cursor.execute('SELECT LEVEL FROM playerdat WHERE USERID=%s LIMIT 1;', (userid,))

	level = conf.level_formula(cursor.fetchone())

	ws = character_sheet.worksheet('Statistics')

	skills = [
			  int(ws.acell(conf.STR).value),
			  int(ws.acell(conf.DEX).value),
			  int(ws.acell(conf.DUR).value),
			  int(ws.acell(conf.MANA).value),
			  int(ws.acell(conf.INTEL).value),
			  int(ws.acell(conf.CHA).value),
			  int(ws.acell(conf.WIS).value),
			  int(ws.acell(conf.PROT).value)
	]

	for skill in skills:
		assert(skill < conf.skill_max() and skill > conf.skill_min)

	cursor.execute('UPDATE playerdat SET STR=%s, DEX=%s, DUR=%s, INTEL=%s, CHA=%s, WIS=%s, PROT=%s, MANA=%s;', tuple(skills))


def update_spreadsheet(userid):
	"""
	Deletes and clones a new spreadsheet, inputting all of the player's data. Used for sheet updates.

	:param: userid: The user's unique Discord ID
	"""

	connection = connect('dbname=userdat.db user=postgres')
	cursor = connection.cursor()

	cursor.execute('SELECT SHEETID FROM playerdat WHERE USERID=%s LIMIT 1;', userid)

	builtins.gogbot.del_spreadsheet(cursor.fetchone())

	cursor.execute('SELECT NAME FROM playerdat WHERE USERID=%s LIMIT 1;', userid)

	new_sheet = builtings.gogbot.copy(conf.MAIN_SHEET, title=f'MCH {cursor.fetchone()} Character Sheet')

	cursor.execute('UPDATE playerdat SET SHEETID=%s WHERE USERID=%s LIMIT 1;', new_sheet.id, userid)
	connection.commit()

	cursor.execute('SELECT EMAIL FROM playerdat WHERE USERID=%s LIMIT 1;', userid)

	load_data(new_sheet)

	new_sheet.share(cursor.fetchone(), perm_type='user', role='writer')

	cursor.close()
	connection.close()

def load_new_default(url):
	sheet = builtins.gogbot.open_by_url(url)