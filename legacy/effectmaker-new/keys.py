##################################################################
### Effectmaker keys.py, last modified by Masterfoxify 3/30/19 ###
##################################################################

### KEYWORDS

# make into a tuple with the second item the name of the parser function prefixed with 'parse_'
# unless if it doesnt have a toplevel parser function, then keep it as a standalone string

KW_IF				= 'IF'
KW_ELSE				= 'ELSE'
KW_END 				= 'END'

TL_KW_CHOOSE		= r'CHOOSE\s+SWITCH', 'choose'
TL_KW_PRINT 		= 'PRINT', 'print'
TL_KW_PARAM 		= 'PARAM', 'param'
TL_KW_IF			= KW_IF, 'conditional'

### EXPRESSION PATTERNS

EX_NUMBER 			= r'\-?[0-9]+\%?'
EX_INT 				= r'\-?[0-9]+'
EX_PERCENT 			= r'\-?[0-9]+\%'
EX_BOOLEAN			= r'TRUE|FALSE'
EX_VARIABLE 		= r'[a-zA-Z_]+'

EX_PLUS				= r'\+'
EX_MINUS 			= r'\-'
EX_TIMES 			= r'\*'
EX_DIVIDED 			= r'\''

EX_AND 				= r'AND'
EX_OR				= r'OR'
EX_NOT 				= r'NOT'

EX_GREATER			= r'\>'
EX_LESS	 			= r'\<'
EX_EQUALS	 		= r'\='
EX_NOT_EQUALS 		= r'\!\='

EX_SWITCH 			= r'SWITCH'
EX_SELF	 			= r'SELF'
EX_ENEMY			= r'ENEMY'

### Effects

# make into a tuple with the second item the name of the relevant stat in the player's dictionaries!!!
# except leave the plain mod as a standalone string

EF_MOD				= r'MOD'
EF_DMG_MOD			= r'DMG\s+MOD', 'dmg'
EF_PHY_MOD			= r'PHY\s+MOD', 'phy'
EF_AURA_MOD	 		= r'AURA\s+MOD', 'aura'
EF__AURA_MOD		= r'STUDY\s+AURA\s+MOD', '_aura'
EF__BODY_MOD		= r'STUDY\s+BODY\s+MOD', '_body'
EF_TITAN_MOD		= r'TITAN\s+MOD', 'titan'
