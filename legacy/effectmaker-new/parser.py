#!/usr/bin/env python3
# this shebang line is just for my convenience, feel free to ignore it lol

import sys
import os
from io import StringIO
import re

import keys

### PARSE & COMPILE ###

# hello! the top level compile function is `compile_effect`
# the way parsing works is by first converting the code string into a stream
# the stream can be read from like a file
# at different stages in parsing, certain patterns are "expected"
# meaning one of those patterns may be read next, otherwise optionally an error will occur
# for example, the statement parser expects any of known keywords that start off a valid statement
# if it sees a PARAM keyword, it calls the PARAM parser
# the PARAM parser then expects to see a series of VARIABLE NAMES
# and so it calls on the VARIABLE NAME parser
# anyway, each parser function returns a function, which represents a fragment of the program
# the top level parser returns a function that executes a BODY
# a BODY is a series of statements to be executed in order
# and as such, the function returned by a BODY parser, executes the functions returned by the STATEMENT parsers in sequence, for each of the statements in that body

# NOTE: some of these parsers operate on full strings, and others operate on the stream
# TODO: rename stream operating functions to "readers" instead of "parsers" for readability
# and keep string operating functions as "parsers"

# keep track of top level patterns associated with parser functions
parsers = {}

def eof(stream):
	""" return whether a stream is at eof yet or not """

	# remember the current position
	pos = stream.tell()

	# read a single char
	char = stream.read(1)

	# backtrack
	stream.seek(pos)

	# return whether eof
	return not char

def parse_multiple(stream, parser, until=None, consume=False):
	""" generator function to yield multiple parsed items in a stream until eof """

	# consume any preceding whitespace
	eat_whitespace(stream)

	# yield statements until eof
	while not eof(stream):

		# optionally look for a terminating pattern
		if until:

			# store the current stream position
			pos = stream.tell()

			# optionally expect any of the terminating patterns
			if expect(stream, until, allow_none=True):

				# backtrack if we aren't to consume it
				if not consume: stream.seek(pos)

				# reached the end
				break

		# yield a statement
		yield parser(stream)

		# consume any preceding whitespace
		eat_whitespace(stream)

def eat_whitespace(stream):
	""" consume any initial whitespace from a stream """

	consume(stream, ' \t\n')

def eat_line_whitespace(stream):
	""" consume any initial whitespace from a stream except newlines """

	consume(stream, ' \t')

def consume(stream, chars):
	""" consume any of given chars from stream """

	# this strikes me as imperfect lol
	# cleaner solution?
	c = chars

	# consume chars as long as they are in the list of chars to be consumed
	while c and c in chars:

		# get the current position in case we need to backtrack
		pos = stream.tell()

		# consume a single char
		c = stream.read(1)

	# since the last char wasnt a char to be consumed,
	# backtrack to just before it
	stream.seek(pos)

def expect(stream, options: list, allow_none=False):
	""" read any of pattern options from stream, error if none found """

	# first record the current stream position for backtracking
	pos = stream.tell()

	# since we cant use regex on a stream
	# we have to read in the rest of the string first...
	# which isnt ideal at all but... shrug
	s = stream.read()

	# iterate through the options and find one
	for option in options:
		if isinstance(option, tuple):
			option = option[0]

		# try if this option matches
		match = re.match(option, s)

		# return the match if so
		if match:

			# get the actual matched string
			matched = match.group(0)

			# set the stream position to the length of the match
			stream.seek(pos + len(matched))

			# and return the pattern and the matched string
			return option, matched

	# restore stream position
	stream.seek(pos)

	# none were found
	# error if that's not allowed
	if not allow_none:

		# TODO: better error messages?
		raise Exception(f'Expected {" or ".join([not isinstance(option, tuple) and option or option[0] for option in options])} at position {pos} but got "{stream.read(16)}"')

def parse_body(stream, until=None, consume=False):
	""" parse a series of statements """

	# parse the statements in the body
	body_statements = list(parse_multiple(stream, parse_statement, until, consume))

	def body(context):
		""" the function to execute the body """

		# just run each statement
		for statement in body_statements:
			statement(context)

	# return the function to execute the body
	return body

def parse_statement(stream):
		""" parse a single statement function """

		# we have to expect there to be any of the possible top level statements here
		option, match = expect(stream, parsers.keys())

		# get the parser function for that keyword as well
		parser = parsers[option]

		# parse the statement and return the function to execute it
		return parser(stream)

def strip_comments(code):
	"""Remove comments from code"""

	# pattern for comments
	# start with '//' and ends with newline
	pattern = r'//.*\n'

	# replace all comments with newlines
	return re.sub(pattern, '\n', code)

def compile_effect(code):
		""" "compile" effect code into a callable function """

		# before anything, strip the code of comments
		code = strip_comments(code)

		# first convert the code string into a stream
		# this way bytes can be "consumed" easily
		stream = StringIO(code)

		# parse the body of the effect program
		body = parse_body(stream)

		def effect(player, target, *arguments):
				""" actually run the program """

				class Context:
						""" holds the context for executing the effect """

						def __init__(self, player, target, arguments):
								self.player = player
								self.target = target
								self.arguments = arguments
								self.vars = {} # keep track of variable values
								self.damage_mod = 1

						def __getitem__(self, key):
								""" return a variable """

								return self.vars[key]

						def __setitem__(self, key, value):
								""" set a variable """

								self.vars[key] = value

				# create a context
				context = Context(player, target, arguments)

				# run the body of the program
				body(context)

		# return the effect function
		return effect
	


### TESTING ###

def test_file(filename, args):
		"""Run an effect from a file on a dummy target using a dummy player"""

		from sys import path
		path.append('../')
		import players

		# the player and target to run the effect with
		player = players.make_dummy('player dummy')
		target = players.make_dummy('target dummy')

		# read the file, compile it, and run it
		with open(filename, 'r') as f:
			print(f'Reading effect code from file "{filename}"...')
			code = f.read()

			print(f'Compiling effect...')
			effect = compile_effect(code)
				
			print(f'Running effect with player "{player}" on target "{target}"...')
			effect(player, target, *args)

#######
### PARSERS
#######

def parse_boolean(string):
	""" parse a boolean value, return a function that returns that value """

	# strip first
	string = string.strip()

	# precompute the value
	# TODO: assert that its either TRUE or FALSE, or nah?
	value = string == 'TRUE'

	# return the evaluation function
	return lambda context: value

def parse_number(string):
	""" parse a numeric value into a function that evaluates it """

	# strip it
	string = string.strip()

	# see if it is a percent or not
	if string[-1] == '%':

		# get the value as a percentage
		value = int(string[:-1]) / 100

	else:

		# get the value normally
		value = int(string)

	# return the evaluation function
	return lambda context: value

def parse_int(string):
	""" parse an int value into a function that evaluates it """

	# strip it
	string = string.strip()

	# get the value normally
	value = int(string)

	# return the evaluation function
	return lambda context: value

def parse_percent(string):
	""" parse an int value into a float then a function that evaluates it """

	# strip it
	string = string.strip()

	# get the value normally
	value = float(string[:-1]) / 100

	# return the evaluation function
	return lambda context: value

def parse_variable(string):
	""" parse a variable name into a function that evaluates it """

	# just return a function that looks it up
	return lambda context: context[string]

def parse_variable_name(stream):
	""" parse a variable name into a string from a stream """

	# expect a variable name from the stream
	pattern, match = expect(stream, [keys.EX_VARIABLE])

	# return the matched string
	return match

def parse_value(stream):
	""" parse a value into a function that evaluates it """

	# the possible things to expect and how to deal with them
	options = {
			keys.EX_NUMBER: parse_number,
			keys.EX_BOOLEAN: parse_boolean,
			keys.EX_VARIABLE: parse_variable,
	}

	# expect one of them
	option, match = expect(stream, options)

	# get the parser for the kind of thing that was found
	parser = options[option]

	# parse the match with it
	value = parser(match)

	# return the "compiled" value
	return lambda context: value(context)

def parse_expression(stream):
	"""Parse an expression"""

	def parse_attribute(stream):
		"""Parse a player object attribute name"""

		# get the var name
		var_name = parse_variable_name(stream)

		# preformat the attribute itself
		attr = f'_{var_name.upper()}'

		# return the function to evaluate it at runtime
		return lambda c: attr

	# expect either an expression with a prefix keyword
	# or if that fails, look for an infix expression
	# match patterns with unary argument parsers and evalutors
	options = {
			keys.EX_NOT:	(parse_expression,	  lambda a, c: not a(c)),
			keys.EX_SWITCH: (parse_variable_name, lambda a, c: c.player._SWITCH.lower() == a.lower()),
			keys.EX_SELF:	(parse_attribute,	  lambda a, c: getattr(c.player, a(c))),
			keys.EX_ENEMY:  (parse_attribute,	  lambda a, c: getattr(c.target, a(c))),
	}

	# expect maybe one of them
	result = expect(stream, options, allow_none=True)

	# if one was found, then parse it with that parser
	if result:

		# unpack the result
		option, match = result

		# get the corresponding parser and evaluator
		parser, evaluator = options[option]

		# consume white space
		eat_whitespace(stream)

		# parse the argument with the corresponding parser
		argument = parser(stream)

		# return the "compiled" expression
		return lambda context: evaluator(argument, context)

	else:

		# otherwise assume its an infix expression and continue from there
		return parse_infix(stream)

def parse_infix(stream):
	""" parse an infix expression into a function that evaluates it """

	# TODO: order of operations!!!
	# TODO: parentheses!

	# first read the left hand side of the expression
	# (and perhaps the entire thing...)
	left = parse_value(stream)

	# eat any whitespace
	eat_whitespace(stream)

	# now expect possibly any of these numerical operators
	options = {
			keys.EX_PLUS:		lambda a, b, c: a(c) + b(c),
			keys.EX_MINUS:		lambda a, b, c: a(c) - b(c),
			keys.EX_TIMES:		lambda a, b, c: a(c) * b(c),
			keys.EX_DIVIDED:	lambda a, b, c: a(c) / b(c),

			keys.EX_EQUALS:		lambda a, b, c: a(c) == b(c),
			keys.EX_NOT_EQUALS:	lambda a, b, c: a(c) != b(c),
			keys.EX_GREATER:	lambda a, b, c: a(c) > b(c),
			keys.EX_LESS:		lambda a, b, c: a(c) < b(c),

			keys.EX_AND:		lambda a, b, c: a(c) and b(c),
			keys.EX_OR:			lambda a, b, c: a(c) or b(c),
	}

	# expect *maybe* one of them
	# it is possible that one isn't there!
	result = expect(stream, options, allow_none=True)

	# if something was found, continue to parse the rest of the expression
	if result:

		# consume white space
		eat_whitespace(stream)

		# parse the right side of the expression
		# (which may recursively be another expression)
		right = parse_expression(stream)

		# unpack the result
		option, match = result

		# get the lambda function for this computation from the dictionary above
		evaluator = options[option]

		# return the "compiled" expression
		return lambda context: evaluator(left, right, context)

	else:

		# return the lone numerical value that was parsed
		return left

def parse_print(stream):
	""" parse a print statement """

	# rememeber, the print keyword has already been consumed
	# consume any preceding whitespace
	eat_whitespace(stream)

	# so now we just need to read an expression to print
	expression = parse_expression(stream)

	def print_func(context):
		""" the print function """

		# evaluate the expression
		value = expression(context)

		# if its a floating point value display it as a rounded percentage
		# idk if you want this
		if type(value) == float:
			print(f'{round(value * 100, 2)}%')

		else:

			# otherwise print it normally
			print(value)

	# return the "compiled" function
	return print_func

def parse_param(stream):
	""" parse a param statement """

	# rememeber, the param keyword has already been consumed
	# consume any preceding whitespace on this same line
	# since this is a statement delimited by a newline
	eat_line_whitespace(stream)

	# read the rest of the line
	line = stream.readline()

	# now parse a series of variable names from this line
	variables = list(parse_multiple(StringIO(line), parse_variable_name))

	def param(context):
		""" the param function """

		# loop through the variables and assign the corresponding values
		for var, value in zip(variables, context.arguments):
			context[var] = value(context)

	# return the "compiled" function
	return param

def parse_conditional(stream):
	""" parse an IF block """

	# rememebr, the if keyword has already been consumed
	# consume and preceding whitespace
	eat_whitespace(stream)

	# now expect an expression which is the condition
	antecedent = parse_expression(stream)

	# now read the body of the if statement
	consequent = parse_body(stream, until=[keys.KW_END, keys.KW_ELSE])

	# see whether we've reached the end or another branch
	option, match = expect(stream, [keys.KW_END, keys.KW_ELSE])

	# if we've reached another branch, parse the alternative
	if match == keys.KW_ELSE:

		# is this the general case, or another condition?
		result = expect(stream, [keys.KW_IF], allow_none=True)

		# if it's another if, then recursively parse another branch
		if result:
			alternative = parse_conditional(stream)

		else:

			# otherwise, simply parse the alternative body
			alternative = parse_body(stream, until=[keys.KW_END], consume=True)

	else:

		# otherwise there is no alternative
		alternative = None

	def branch(context):
		""" the branch execution function """

		# only execute the consequent if the antecedent is true
		if antecedent(context):
			consequent(context)

		else:

			# otherwise execute the alternative if it exists
			if alternative: alternative(context)

	# return the "compiled" if statement
	return branch

def parse_choose(stream):
	"""Parse a CHOOSE SWITCH statement"""

	def choose(context):
		"""Run the choose statement"""

		# grab a vaue from the console for now
		switch = input('Choose switch: ')

		# set the switch in the player object
		context.player._SWITCH = switch

	# return the "compiled" choose statement
	return choose

#######
### AFFLICTIONS
#######

def affliction(pattern, *paramater_parsers):
	"""Return an affliction parser that parses some number of args"""

	def decorator(func):
		"""The decorator"""

		# func = the "compiled" function
		# this decorator needs to return a function that parses `args`
		# which in turns returns `func`

		def parser(stream):
			"""Parse this particular affliction"""

			def parse(parser):
				"""Parse an argument with a parser"""

				# first consume whitespace preceding the argument
				eat_whitespace(stream)

				# then use the parser to parse the argument
				return parser(stream)

			# parse the arguments
			args = list(map(parse, paramater_parsers))

			def closure(context):
				"""Create a closure for the args"""

				return func(context, *args)

			# return the "compiled" function (inside the closure)
			return closure

		# set the parser for this pattern and return the parser
		parsers[pattern] = parser

		return parser

	return decorator

@affliction('ADVANTAGE')
def parse_advantage(context):
	context.target.apply_bonus('advantage', True, turns=1)

@affliction('BIND', parse_int)
def parse_bind(context, turns):
	context.target.apply_bonus('range', -2, turns=turns)
	context.target.apply_bonus('hit', -5, turns=turns)

@affliction('BLEED', parse_percent, parse_int)
def parse_bleed(context, amount, turns):
	context.target.buffed['armor'].append((turns, amount))

@affliction('BLIND', parse_int)
def parse_blind(context, turns):
	context.target.apply_override('range', 0, turns=turns)

@affliction('BOOST', parse_percent)
def parse_boost(context, boost):
	context.target.apply_bonus('damage', boost, turns=1)

@affliction('BLOCK', parse_int)
def parse_block(context, length):
	context.target.block = length

@affliction('BREAK\s+MAGIC')
def parse_break_magic(context):
	context.channel = 0

@affliction('BURN', parse_int)
def parse_burn(context, percent, turns):
	context.target.bonuses['shield'].append((turns, percent))

@affliction('BURROW', parse_int)
def parse_burrow(context, turns):
	context.target.apply_bonus('burrow', True, turns=turns)
	# lol not really a bonus

@affliction('CHANNEL|CHARGE', parse_int)
def parse_channel(context, turns):
	context.target.channel = turns

@affliction('CLEANSE')
def parse_cleanse(context):
	context.target.bonuses = {}

@affliction('CONFUSION', parse_int, parse_int, parse_int)
def parse_confusion(context, dc, d, turns):
	context.target.confusions.append({
		'dc': dc,
		'd': d,
		'turns': turns,
	})

@affliction('DISADVANTAGE')
def parse_disadvantage(context):
	context.target.apply_bonus('disadvantage', True, turns=1)

@affliction('DISPLACE', parse_int, parse_int)
def parse_displace(context, spaces, direction):
	pass
	# TODO: Finish this lol

@affliction('DISTRACT', parse_int, parse_int)
def parse_distract(context, reduction, turns):
	context.target.apply_bonus('rolls', -abs(reduction), turns=turns)

@affliction('DRAGON\'S\s+EYE', parse_percent, parse_int)
def parse_dragons_eye(context, boost, turns):
	context.target.buffed['detection'].append((turns, boost))

@affliction('DROWSY')
def parse_drowsy(context):
	pass
	# TODO: Probably create for first expansion

@affliction('FEAR', parse_int)
def parse_fear(context, spaces):
	pass
	# TODO: Grid implementation, possibly for first expansion

@affliction('FLOAT')
def parse_float(context):
	pass
	# TODO: Hazard implementation, probably for first expansion

@affliction('FOCUS', parse_int)
def parse_focus(context, bonus)
	context.target.apply_bonus('hit', bonus, turns=turns)

@affliction('FROZEN')
def parse_frozen(context):
	pass
	# TODO: true afflictions like Drowsy and Frozen, probably for first expasion

@affliction('GEASS')
def parse_geass(context):
	pass
	# TODO: Grid implementation

@affliction('GRAB')
def parse_grab(context):
	pass
	# TODO: Grid implementation

@affliction('GRAPPLE')
def parse_grapple(context):
	pass
	# TODO: Grid implementation

@affliction('GROWTH', parse_percent, parse_int)
def parse_growth(context, boost, turns):
	context.target.channel = turns
	# TODO: Buff attack? Also missing in MOD

@affliction('HARDEN', parse_percent, parse_int)
def parse_harden(context, boost, turns):
	context.target.buffed['armor'].append((turns, boost))

@affliction('HASTE')
def parse_haste(context):
	pass
	# TODO: Decide on haste implementation. Dodge roll or movement?

@affliction('HAZARD')
def parse_hazard(context):
	pass
	# TODO: Probably hazard implementation in first expansion

@affliction('HAZE', parse_int, parse_int)
def parse_haze(context, reduction, turns)
	pass
	# Unsure how to implement tbh

@affliction('ILLUSION', parse_int)
def parse_illusion(context, turns):
	pass
	# Unsure how to implement as well

@affliction('KNOCKUP')
def parse_knockup(context):
	pass
	# TODO: Probably implement in first expansion alongside burrow and fly

@affliction('LAND')
def parse_land(context):
	pass
	# TODO: Implement in first expansion alongside burrow and fly

@affliction('LEECH|LIFESTEAL', parse_percent)
def parse_leech(context, amount):
	context.target.apply_bonus('leech', amount, turns=1)

@affliction('LOVE')
def parse_love(context):
	pass
	# Rename, please?

@affliction('MANA\s+DEFICIENCY', parse_percent, parse_int)
def parse_deficiency(context, reduction, turns):
	for buff in context.target.buffed.keys():
		if buff.startswith('_'):
			context.target.buffed[buff].append((turns, 1 - reduction))

@affliction('MANA\s+DRAIN', parse_percent)
def parse_drain(context, percent):
	mana = int(context.target._CURRENTMANA * (1 - percent))
	context.target._CURRENTMANA = int(context.target._CURRENTMANA * (1 - percent))
	context.player._CURRENTMANA += mana

@affliction('MANA\s+EFFICIENCY', parse_percent, parse_int)
def parse_efficiency(context, boost, turns):
	for buff in context.target.buffed.keys():
		if buff.startswith('_'):
			context.target.buffed[buff] = (turns, 1 + boost)

@affliction('MANA\s+ENDOWMENT', parse_percent)
def parse_endowment(context, percent):
	pass
	# TODO: Add amount of Mana invested into context

@affliction('MANA\s+GATE', parse_percent)
def parse_gate(context, boost):
	context.target.buffed['invest'].append((1, 1 + boost))

@affliction('MANA\s+PRESSURE', parse_percent, parse_int)
def parse_pressure(context, reduction, turns):
	context.target.buffed['invest'].append((turns, reduction))

@affliction('MANA\s+RESTORATION', parse_percent, parse_int)
def parse_restoration(context, amount, turns):
	context.target.channel = turns
	# Unkown how to have something happen only after channeling

@affliction('MIXED')
def parse_mixed(context):
	pass
	# Unkown if should implement here

@affliction('MULTI\s+HIT', parse_percent, parse_int)
def parse_multi_hit(context):
	pass
	# Unsure how to implement




#######
### Effects
#######

def parse_mod(stream, prefix=None):
	"""Parses a mod statement"""

	# remember, the keyword has already been consumed
	# now consume the whitespace between "MOD" and the arguments
	eat_whitespace(stream)

	# first get the expected mod value
	# now expect either an int or a percent for the value
	options = {
			keys.EX_INT: parse_int,
			keys.EX_PERCENT: parse_percent,
	}

	# get whatever is there
	value_type, match = expect(stream, [keys.EX_PERCENT, keys.EX_INT])

	# get the parser function for parsing that value
	parser = options[value_type]

	# parse it
	value = parser(match)

	# consume any whitespace
	eat_whitespace(stream)

	# now we need to optionally expect a turn amount number
	result = expect(stream, [keys.EX_INT], allow_none=True)

	# default to 1 turn if no amount was specified
	# TODO: put some bounds checking on this probably
	turns = parse_int(result[1]) if result else lambda c: 1

	# the "compiled" fuction
	def mod(context):
		"""This is run at the effect's runtime"""

		# prefix is any keyword preceding "MOD" if any
		# and value is the value parsed after "MOD"
		# to access that value, call it like value(context)
		# (this is because it may be a variable whose value is only known at runtime)
		# context.player is the player instance who used this effect
		# context.target is the player instance that was targeted

		# is this a MOD with a preceding keyword?
		if prefix:

			# the data to append
			data = { turns(context): value(context) }

			# the list to append it to
			dest = context.target.static_buffed if value_type == keys.EX_PERCENT else context.target.buffed

			# append it!
			try:
					dest[prefix].append(data)

			except KeyError as e:
				print(f'Undefined stat used with mod: {e}')

		else:

			# change context mod
			context.damage_mod *= value(context)

	# return the "compiled" function
	return mod

functions = locals()

for item in dir(keys):
	value = getattr(keys, item)

	# item = the variable name in keys.py
	# value = the value of that variable
	
	# if a toplevel KEYWORD, and is a tuple, then add a parser for it
	# if its not a tuple, that mean it doesnt have a top level parser
	if item.startswith('TL_KW_') and isinstance(value, tuple):

		# get the pattern and parser function
		pattern, func_name = value

		# get the parser function itself
		parser = functions.get(f'parse_{func_name}')
		
		# add a parser to parse the pattern
		parsers[pattern] = parser

	elif item.startswith('EF_'):

		# if its an EFFECT keyword, add a MOD parser
		# is it a standalone MOD or with a prefix?
		# prefix'd ones are a tuple
		if isinstance(value, tuple):

			# get the pattern and stat name
			pattern, stat = value

			# create a closure with the stat value in order to 'freeze' it as is
			# otherwise the stat value will change for each iteration of this loop!
			def closure(stat):
				return lambda stream: parse_mod(stream, stat)

			# add the parser
			parsers[pattern] = closure(stat)

		else:

			# this is for standalone mod
			parsers[value] = parse_mod

# for testing, run this file directly with filename
if __name__ == '__main__':

	if len(sys.argv) > 1:
		test_file(sys.argv[1], list(map(parse_number, sys.argv[2:])))

	else:
		print(f'Usage: python {sys.argv[0]} effect.txt')
