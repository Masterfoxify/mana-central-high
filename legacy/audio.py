# Discord imports
import discord
from discord.ext import commands

# Built-in imports
from sys import path
from os import listdir
from random import choice

path.append('../')

# In-house imports
import main
from lib import botlogger as log
import config as conf

logging_music = log.Logger(log.Configuration('music'))

class Music:
	def __init__(self, bot):
		self._bot = bot
		self.voice_client = None
		self.shuffled = []
		self.playlist = []
		self.currently_playing = ''

	def shuffler(self):
		"""Shuffler for the voice client"""

		if not self.voice_client is None:
			if len(self.shuffled) == len(self.playlist):
				self.shuffled = [self.currently_playing]

			song = choice(list(set(self.shuffled) ^ set(self.playlist)))

			self.shuffled.append(song)

			self.currently_playing = song

			logging_music.debug('Playlist: ' + str(self.playlist))
			logging_music.debug('Shuffle: ' + str(self.shuffled))
			logging_music.debug('Symmetric Difference: ' + str(list(set(self.shuffled) ^ set(self.playlist))))
			logging_music.debug('Song: ' + song)

			with open(f'music/{song}') as f:
				self.voice_client.play(discord.FFmpegPCMAudio(f, pipe=True), after=lambda error: shuffler())

	@commands.group()
	async def music(self, ctx):
		"""- Music command group. Do !help music for commands"""
		pass

	@music.command()
	async def join(self, ctx):
		"""- Tells the bot to join the VC channel you're in"""

		if ctx.author.voice.channel is None:
			await ctx.send(f'Sorry {ctx.author.mention}, you are not in a voice channel.')
			logging_music.verbose(f'Bot called to join VC, but {ctx.message.author.name} was not in a voice channel.')

		else:
			self.voice_client = await ctx.author.voice.channel.connect()
			logging_music.send(f'Bot now in VC {ctx.author.voice.channel.name}.')

	@music.command()
	async def shuffle(self, ctx):
		"""- Shuffles through the default playlist"""
		
		if self.voice_client is None:
			await ctx.send('I am not currently in a voice channel. Please use `!music join` while in a voice channel to get me to join your voice chat!')
			logging_music.verbose(f'Shuffle attempted by {ctx.message.author.name}, but bot is not currently in a voice channel.')

		else:
			self.playlist = listdir('music')
			self.shuffler()
			await ctx.send(f'Music shuffle started by {ctx.message.author.name}')
			logging_music.send(f'Music shuffle started by {ctx.message.author.name}')

	@music.command()
	async def pause(self, ctx):
		"""- Pauses the audio player"""

		if self.voice_client is None or not self.voice_client.is_playing():
			await ctx.send('I am not currently playing any music.')
			logging_music.verbose(f'Pause called by {ctx.message.author.name} but no music was playing.')

		else:
			self.voice_client.pause()
			await ctx.send(f'Music paused by {ctx.message.author.name}')
			logging_music.send(f'Music paused by {ctx.message.author.name}.')

	@music.command()
	async def stop(self, ctx):
		"""- Stops the audio player"""

		if self.voice_client is None:
			await ctx.send('I am not currently in a voice channel.')
			logging_music.verbose(f'Stop called by {ctx.message.author.name} but bot was not in a voice channel.')

		else:
			self.shuffled = []
			self.voice_client.stop()
			await ctx.send(f'Music stopped by {ctx.message.author.name}')
			logging_music.send(f'Music stopped by {ctx.message.author.name}.')

			self.currently_playing = ''

	@music.command()
	async def resume(self, ctx):
		"""- Resumes the audio player"""

		if self.voice_client is None:
			await ctx.send('I am not currently in a voice channel.')
			logging_music.verbose(f'Resume called by {ctx.message.author.name} but bot was not in a voice channel.')

		elif self.voice_client.is_playing():
			await ctx.send('I am already playing music.')
			logging_music.verbose(f'Resume called by {ctx.message.author.name} but bot was already playing music.')

		else:
			self.voice_client.resume()
			await ctx.send(f'Music resumed by {ctx.message.author.name}')
			logging_music.send(f'Music resumed by **{ctx.message.author.name}**.')

	@music.command()
	async def skip(self, ctx):
		"""- Skips the current song"""

		logging_music.send(f'{ctx.message.author.name} has skipped {self.currently_playing}.')

		self.voice_client.stop()

		await ctx.send(f'**{ctx.message.author.name}** has skipped **{self.currently_playing}**.')
		self.shuffler()


	@music.command()
	async def leave(self, ctx):
		"""- Tells the bot to leave the voice channel :("""

		if self.voice_client is None:
			await ctx.send('I am not currently in a voice channel.')
			logging_music.verbose(f'Leave called by {ctx.message.author.name} but bot was not in a voice channel.')

		else:
			logging_music.send(f'{ctx.message.author.name} called bot to leave {self.voice_client.channel.name}.')

			self.voice_client.stop()
			await self.voice_client.disconnect()
			await ctx.send('I have left the voice channel.')
			self.currently_playing = ''

	@music.command()
	async def playing(self, ctx):
		"""- Returns the currently playing track"""

		if self.currently_playing == '':
			await ctx.send('I am not currently playing anything.')

		else:
			await ctx.send(self.currently_playing)


def setup(bot):
	bot.add_cog(Music(bot))