import random
from sys import path
path.append('../')

from main import get_cache

# Non-message based rolling. Returns dictionary of rolls plus their sum or a single roll if it's only one die.
def auto_roll(dice_amount: int, dice_type: int, add: int=0, sub: int=0, overall: bool=False, adv: bool=False, disadv: bool=False) -> dict:
	"""
	The all-in-one dice roller.
	If the dice_amount == 1, it returns a single number instead of an array.

	:param: `dice_amount`: The amount of dice being "thrown"
	:param: `dice_type`: The type of dice being thrown (ex: d20)
	:param: `add`: The amount to add to the roll(s)
	:param: `sub`: The amount to sub to the roll(s)
	:param: `overall`: Whether the roll is overall or not. Defaults to false
	:param: `adv`: Whether the roll has advantage
	:param: `disadv`: Whether the roll has disadvantage
	"""

	# If the dice amount == 1, then just do a single roll
	if dice_amount == 1:

		# Checks for advantage
		if adv==True:

			# Builds a single die roll, then another for advantage check
			roll.append(random.choice(range(1, dice_type + 1)))
			roll2.append(random.choice(range(1, dice_type + 1)))

			# Simple check to see if roll is greater than roll2
			roll = roll > roll2 and roll or roll2

			# Adds add and subs sub
			roll += add
			roll -= sub
			return roll

		# Checks for disadvantage
		elif disadv==True:

			# Builds a single die roll, then another for disadvantage check
			roll.append(random.choice(range(1, dice_type + 1)))
			roll2.append(random.choice(range(1, dice_type + 1)))

			# Simple check to see if roll is less than roll2
			roll = roll < roll2 and roll or roll2

			# Adds add and subs sub
			roll += add
			roll -= sub
			return roll

		else:

			# Builds a single die roll
			roll = random.choice(range(1, dice_type + 1))

			# Adds add and subs sub
			roll += add
			roll -= sub
			return roll

	else:

		# Checks for advantage
		if adv==True:

			# Creates empty lists of rolls
			rolls = []
			rolls2 = []

			# Iterates through the amount of dice
			for die in range(dice_amount):
				rolls.append(random.choice(range(1, dice_type + 1)))
				rolls2.append(random.choice(range(1, dice_type + 1)))

			# Builds a list of dice rolls, then another for advantage check
			rolls = sum(rolls) > sum(rolls2) and rolls or rolls2

			# If overall is true then add at the end
			if overall == True:
				return {'rolls': sorted(rolls), 'sum': sum(rolls) + add - sub}

			# If overall is false, then add to each individual roll
			else:
				return {'rolls': sorted([roll + add - sub for roll in rolls]), 'sum': sum(rolls) + add * len(rolls) - sub * len(rolls)}

		# Checks for disadvantage
		elif disadv==True:

			# Creates empty lists of rolls
			rolls = []
			rolls2 = []

			# Iterates through the amount of dice
			for die in range(dice_amount):
				rolls.append(random.choice(range(1, dice_type + 1)))
				rolls2.append(random.choice(range(1, dice_type + 1)))

			# Builds a list of dice rolls, then another for disadvantage check
			rolls = sum(rolls) < sum(rolls2) and rolls or rolls2

			# If overall is true then add at the end
			if overall == True:
				return {'rolls': sorted(rolls), 'sum': sum(rolls) + add - sub}

			# If overall is false, then add to each individual roll
			else:
				return {'rolls': sorted([roll + add - sub for roll in rolls]), 'sum': sum(rolls) + add * len(rolls) - sub * len(rolls)}

		else:

			# Creates empty list of rolls
			rolls = []

			# Iterates through the amount of dice
			for die in range(dice_amount):
				rolls.append(random.choice(range(1, dice_type + 1)))

			# If overall is true then add at the end
			if overall == True:
				return {'rolls': sorted(rolls), 'sum': sum(rolls) + add - sub}

			# If overall is false, then add to each individual roll
			else:
				return {'rolls': sorted([roll + add - sub for roll in rolls]), 'sum': sum([roll + add - sub for roll in rolls])}

def roll_through_message(message):
	"""
	Allows players to roll through messages, instead of through spells or other API.
	
	Returns an instance of :meth:`lib.diceroller.auto_roll`
		
	Usage examples:\n
	!roll 1d10 (returns number 1-10)\n
	!roll 2d5+2^ (returns two numbers between 1-5, then only adds the +2 for the sum, not individual rolls)\n
	!roll 1d10+2>^ (returns a number between 1-10 with advantage, and adds the +2 only for the sum)\n
	!roll d20+5< (returns a number between 1-20, adds a 5 to each rolls, then calculates disadvantage)

	:param: `message`: The message that called this. Used for dice parsing"""

	dice_amount = 0
	dice_type = 0
	add = 0
	sub = 0
	overall = False
	adv = False
	disadv = False
	msg = message.content.replace('!roll ', '')
	msg = msg.replace('!initiative ', '')

	if '>' in msg:
		adv = True
		msg = msg.replace('>', '')

	elif '<' in msg:
		disadv = True
		msg = msg.replace('<', '')

	if '^' in msg:
		overall = True
		msg = msg.replace('^', '')

	if '+' in msg:
		if ' d' in message.content:
			dice_amount = 1
		else:
			dice_amount = msg.split('d')[0]
		dice_type = msg.split('d')[1]
		dice_type = int(dice_type.split('+')[0])
		
		add = msg.split('+')[1]
		sub = 0

	elif '-' in msg:
		if ' d' in message.content:
			dice_amount = 1
		else:
			dice_amount = msg.split('d')[0]
		dice_type = msg.split('d')[1]
		dice_type = int(dice_type.split('-')[0])

		sub = msg.split('-')[1]
		add = 0

	else:
		if ' d' in message.content:
			dice_amount = 1
		else:
			dice_amount = msg.split('d')[0]
		dice_type = int(msg.split('d')[1])

		add = 0
		sub = 0

	return auto_roll(int(dice_amount), int(dice_type), add=int(add), sub=int(sub), overall=overall, adv=adv, disadv=disadv)

def does_hit(channel_id, player_Object, enemy):
	""":warning: DEPRECATED

	Calculates whether a Player hits the Enemy or not. Returns True if it hits, else False if it misses.

	:param: `channel_id`: The channel ID this is being called from. Used to access combat cache location
	:param: `player_Object`: An object of class :class:`Player` that is attacking
	:param: `enemy`: An object of class :class:`Player` that is getting hit
	"""

	local_cache = get_cache()

	try:
		chance_to_hit = local_cache[channel_id].get_hit_chance(player_Object, enemy) + Player_object.bonuses['roll']
		chance_to_dodge = enemy.bonuses['dodge']

		attack_roll = auto_roll(1, 20, chance_to_hit, adv = Player_object.status['atk_adv'], disadv = Player_object.status['atk_disadv'])
		defense_roll = auto_roll(1, 20, chance_to_dodge, adv = enemy.status['dodge_adv'], disadv = enemy.status['dodge_disadv'])

		return attack_roll >= defense_roll

	except (OverflowError, ZeroDivisionError):
		return None