import imgkit

# Pathfinding imports
from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid as PFGrid
from pathfinding.finder.a_star import AStarFinder

class OutOfBoundsException(Exception):
	pass

class ObstructionException(Exception):
	pass


container = lambda picture: f"<div style=\"width: 240;height: 240;border-radius: 50%;margin: 0 0 auto\">" + (picture is not None and f"<img src={picture}>" or "") + "</div>"

def create_row(x_offset, *args) -> str:
	"""Creates an HTML row of squares"""

	output = ""
	i = 0
	for arg in args:
		output += f"<div style=\"position: absolute;left: {x_offset};top: {380 * i};width: 360;height: 360;background-color: black;margin: 0 0 0 0;padding: 10px 10px 10px 10px;border-color: white;border-width: 5px;border-style: solid;\">{container(arg)}</div>"
		i += 1

	return output

class Grid:
	"""Grid object. An x*y sized grid of :class:`Cell` objects."""

	def __init__(self, *players, height: int = 25, width: int = 25):
		self.height = height
		self.width = width
		self.cells = [[Cell() for x in range(width)] for y in range(height)]
		self.players = {}

		for player in players:
			self.players[player.get_name()] = (-1, -1)


	"""
		Table functions
	"""


	def get_table(self) -> str:
		"""Returns the table as HTML"""

		return self.create_table(self)

	def render_table(self):
		"""Returns an image of the table"""

		return imgkit.from_string(f'<html><body>{self.create_table()}</body></html>', False)

	def create_table(self) -> str:
		"""Creates the table row by row then returns the output"""

		output = ''

		# Iterate through the entire grid
		for y in range(self.height):
			row = []

			for x in range(self.width):
				if self.cells[y][x].player is not None:

					# Get player image and append it
					row.append(self.cells[x].get_image())

				else:
					row.append(None)

			output += create_row(380 * y, *row)

		return output


class Combat:
	def __init__(self, grid: Grid, channel, *players: list):
		"""
		Combat wrapper class.

		:param: `grid`: The combat grid
		:param: `players`: An *args of players
		"""
		self.players = players
		self.grid = grid
		self.active = True

		players.sort(key=lambda player: player.get_initiative())

		while active:
			initiative = get_initiative()

	def get_initiative():
		"""Returns the initiative using dict comprehension"""
		return {player.get_name(): player.get_initiative() for player in self.players} # TODO: get sorted working properly


	"""
		Mathematical Functionality
	"""


	def get_position(self, player_name: str) -> tuple:
		return this.players[player_name]


	def get_closest_player(self, main: str) -> str:
		closest = ""
		closest_dist = 0

		for player, distance in self.players:
			distance_calc = abs((distance[0] - self.players[main][0]) / (distance[1] - self.players[main][1]))

			if distance_calc < closest_dist or closest_dist == 0:
				closest = player
				closest_dist = distance_calc

		return closest


	def get_distance(self, player1, player2):
		return abs((self.players[player2][0] - self.players[player1][0]) / (players[player2][1] - self.players[player2][1]))


	def get_matrix(self, target):
		matrix = []

		for y in self.cells:
			matrix.append([])

			for x in self.cells:
				matrix[y].append(not (self.cells[y][x].has_object and self.cells[y][x].has_player) and 1 or 0)

		matrix[target[0]][target[1]] = 1
		return matrix


	def pathfind(x: int, y: int, main: str, target: str):
		pfgrid = PFGrid(matrix = self.get_matrix)
		start = pfgrid.node(*self.get_position(main))
		end = pfgrid.node(*self.get_position(target))

		finder = AStarFinder(diagonal_movement=DiagonalMovement.always)
		path, runs = finder.find_path(start, end, grid)

		return path


	"""
		Cell/Movement Functionality
	"""


	def move_player(self, player_name: str, amt_up: int, amt_right: int):
		"""
		Moves player

		:param: `player_name`: Player's name
		:param: `amt_up`: Amount up. Can be negative
		:param: `amt_right`: Amount right. Can be negative

		:exception: `OutOfBoundsException`: Raised when player is out of bounds
		:exception: `ObstructionException`: Raised when player is obstructed
		"""

		coords = get_position(player_name)

		# Check to make sure movement is not out of bounds
		if not -1 < coords[0] + amt_up < self.height or not -1 < coords[1] + amt_right < self.width:
			raise OutOfBoundsException('Movement is out of bounds.')

		if self.cells[coords[0], coords[1]].has_object():
			raise ObstructionException('Object is in the way.')

		# Swap the two cells' players
		self.cells[coords[0], coords[1]].player, self.cells[coords[0] + amt_up, coords[1] + amt_right].player = self.cells[coords[0] + amt_up, coords[1] + amt_right].player, self.cells[coords[0], coords[1]].player


	"""
		Player Functionality
	"""


	def get_hit_chance(player1, player2, ranged: bool = False) -> int:
		"""
		Returns the raw hit chance for one player to hit another

		Ranged:\n\t
			https://www.desmos.com/calculator/cg0fudyxiv

		Melee:\n\t
			https://www.desmos.com/calculator/seoehhph44

		:param: `player1`: The player attempting to hit
		:param: `player2`: The player getting hit

		:param: `ranged`: If the attack is ranged. Defaults to False
		"""

		x = grid.get_distance(player1, player2)
		a = player1.get_level()
		b = player1.get_dex()

		if ranged:
			return int(-(((1 / 20) * x) / (a ** (1 / 50)) - 2.7 + a ** (1 / 50)) ** 4 + 4 + (1 / 50) * a + b / 500)

		else:
			return int(-(x * (1 / a) / ((1 / x ** x) * b * (5 * a)) / a ** -2) ** 10 + ((1 / 10) * a) / (a ** (1 / 50)) + 4)


class Cell:
	"""Cell data class for Grid."""

	def __init__(self, player = None, obj = None, glyphs: list = None):
		self.player = player
		self.has_object = lambda: obj is not None
		self.has_player = lambda: player is not None
		self.glyphs = glyphs