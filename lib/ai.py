class Node:
	def __init__(self, x: int, y: int):
		self.x = x
		self.y = y

class AI:
	def __init__(self, atk_range: int, movement: int, aggressiveness: int = 50, tactical: int = 50):
		self.atk_range = atk_range
		self.movement = movement
		self.aggressiveness = aggressiveness
		self.tactical = tactical


	def get_nodes(grid, coordinates, target) -> tuple:
		nodes = ()
		active_nodes = []

		nodes[0] = Node(coordinates[0], coordinates[1])
		active_nodes[0] = Node(coordinates[0], coordinates[1])
		player_found = False

		rise = (coordinates[0] - target[0])
		run = (coordinates[1] - target[1])

		while not player_found:
			for node in active_nodes:

				player_found = (node.y + rise, node.x + run) == target

				if not grid.cells[node.y + rise][node.x + run].has_object:
					active_nodes.remove(node)

					new_node = Node(node.x + run, node.y + rise)

					active_nodes.append(new_node)
					nodes.append(new_node)

				else:
					possible_coordinates = ((node.x + 1, node.y + 1), (node.x + 1, node.y),
											(node.x, node.y + 1), (node.x, node.y - 1), (node.x - 1, node.y),
											(node.x - 1, node.y - 1))

					for possibility in possible_coordinates:
						if not grid.cells[node.y + rise][node.x + run].has_object and Node(possibility[0], possibility[1]) not in nodes:
							new_node = Node(node.x + run, node.y + rise)

							active_nodes.append(new_node)
							nodes.append(new_node)


	def pathfind()