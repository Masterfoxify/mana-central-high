from psycopg2 import connect
from sys import path
import math

path.append('.. / ')

def make_dummy(name):
	"""Instantiate a dummy player object for testing purposes"""

	stats = [0, 'dummy@somewhere.com', 0, name]
	stats += [0] * 13
	stats += [1, 0, 0, 0, 0]
	stats += [1]
	stats += [0] * 11
	stats += [5] * 8
	stats += [{}]
	stats += [1000, 1000, 1000]
	return Player(*stats)

class NameException(Exception):
	pass

class Bonus:
	def __init__(self, value, turns=1):
		self.value = value
		self.turns = turns

class Player:
	"""Player data wrapper class for combat."""

	def __init__(self, *stats, initiative=0):
			"""
					All stats are uppercase not because they're constants, but because they're named that to match their SQL counterparts.

					:param: `stats`: The stats to be used for the player. The only things stored are the SQL values, everything else is dynamically created from those.
					:param: `initiative`: The player's initiative. Defaults to 0.
			"""

			self._USERID = stats[0]
			self._NAME = stats[3]
			self._EXP = stats[4]
			self._RANK = stats[5]
			self._AGE = stats[6]
			self._BIRTHDAY = stats[7]
			self._HEIGHT = stats[8]
			self._YEAR = stats[9]
			self._GENDER = stats[10]
			self._AURA = stats[11]
			self._IMAGE1 = stats[12]
			self._IMAGE2 = stats[13]
			self._THEME = stats[14]
			self._INFO = stats[15]
			self._BACKSTORY = stats[16]

			self._PROTECTOR = stats[17]
			self._MAGE = stats[18]
			self._WARRIOR = stats[19]
			self._CLERIC = stats[20]
			self._HERO = stats[21]

			self._TITAN = stats[22]
			self._PALADIN = stats[23]
			self._BERSERK = stats[24]
			self._WIZARD = stats[25]
			self._SAGACIOUS = stats[26]
			self._SORCERER = stats[27]
			self._KNIGHT = stats[28]
			self._DEFENDER = stats[29]
			self._MERCENARY = stats[30]
			self._PRIEST = stats[31]
			self._ASSASSIN = stats[32]
			self._BANDIT = stats[33]
			self._BASIC = 0

			self._STR = stats[34]
			self._DEX = stats[35]
			self._DUR = stats[36]
			self._INT = stats[37]
			self._CHA = stats[38]
			self._WIS = stats[39]
			self._PROT = stats[40]
			self._MANA = stats[41]

			self._ABILITIES = dict(stats[42])

			self._CURRENTAURA = stats[43]
			self._CURRENTHEALTH = stats[44]
			self._CURRENTMANA = stats[45]

			self._SWITCH = ''
			self._SWITCHB = ''

			self.buffed = {'aura': [], 'armor': [], 'shield': [], 'phy': [], 'mag': [], 'reserve': [], 'detection': [], 'control': [], 'recovery': [], 'invest': [],
									   'str': [], 'dex': [], 'dur': [], 'int': [], 'cha': [], 'wis': [], 'prot': [], 'mana': [],
									   '_body': [], '_transformation': [], '_aura': [], '_creation': [], '_destruction': [], '_life': []}

			self.static_buffed = {'aura': [], 'armor': [], 'shield': [], 'phy': [], 'mag': [], 'reserve': [], 'detection': [], 'control': [], 'recovery': [], 'invest': [],
													  'str': [], 'dex': [], 'dur': [], 'int': [], 'cha': [], 'wis': [], 'prot': [], 'mana': [],
													  '_body': [], '_transformation': [], '_aura': [], '_creation': [], '_destruction': [], '_life': [],

													  'protector': [], 'mage': [], 'warrior': [], 'cleric': [], 'hero': [],

													  'titan': [], 'paladin': [], 'berserk': [], 'wizard': [], 'sagacious': [], 'sorcerer': [], 'knight': [], 'defender': [],
													  'mercenary': [], 'priest': [], 'assassin': [], 'bandit': [], 'basic': []}

			self.bonuses = {}
			self.overrides = {}
			self.block = None
			self.channel = 0 # turns
			self.confusions = []

			self.initiative = initiative

	def get_override(self, override):
		"""Get a override list, make it if it doesnt exist"""

		if override not in self.overrides:
			self.overrides[override] = []

		return self.overrides[override]

	def get_bonus(self, bonus):
		"""Get a bonus list, make it if it doesnt exist"""

		if bonus not in self.bonuses:
			self.bonuses[bonus] = []

		return self.bonuses[bonus]

	def apply_override(self, override, value, turns=1):
		"""Apply a override"""

		self.get_override(override).append(Bonus(value, turns))

	def apply_bonus(self, bonus, value, turns=1):
		"""Apply a bonus"""

		self.get_bonus(bonus).append(Bonus(value, turns))

	def __str__(self):
			""" stringify this dummy player using the player's name"""
			return self._NAME

	"""
			Getters

			This is necessary since many stats in this game are dynamically calculated
	"""

	def get_userID():
			return self._USERID

	def get_name():
			return self._NAME

	def get_exp():
			return self._EXP

	def get_rank():
			return self._RANK

	def get_age():
			return self._AGE

	def get_birthday():
			return self._BIRTHDAY

	def get_height():
			return self._HEIGHT

	def get_year():
			return self._YEAR

	def get_gender():
			return self._GENDER

	def get__aura():
			"""This one specifically has two underscores. Remember this!"""
			return self._AURA

	def get_image1():
			return self._IMAGE1

	def get_image2():
			return self._IMAGE2

	def get_theme():
			return self._THEME

	def get_info():
			return self._INFO

	def get_backstory():
			return self._BACKSTORY

	def get_protector():
			return self._PROTECTOR

	def get_mage():
			return self._MAGE

	def get_warrior():
			return self._WARRIOR

	def get_cleric():
			return self._CLERIC

	def get_hero():
			return self._HERO

	def get_titan():
			return self._TITAN

	def get_paladin():
			return self._PALADIN

	def get_berserk():
			return self._BERSERK

	def get_wizard():
			return self._WIZARD

	def get_sagacious():
			return self._SAGACIOUS

	def get_sorcerer():
			return self._SORCEROR

	def get_knight():
			return self._KNIGHT

	def get_defender():
			return self._DEFENDER

	def get_mercenary():
			return self._MERCENARY

	def get_priest():
			return self._PRIEST

	def get_assassin():
			return self._ASSASSIN

	def get_bandit():
			return self._BANDIT

	def get_str():
			return self._STR

	def get_dex():
			return self._DEX

	def get_dur():
			return self._DUR

	def get_int():
			return self._INT

	def get_cha():
			return self._CHA

	def get_wis():
			return self._WIS

	def get_prot():
			return self._PROT

	def get_mana():
			return self._MANA

	def get_abilities():
			return self._ABILITIES


	"""
			Unique Getters
	"""

	"""
			Classes
	"""

	"""
			Protector
	"""

	def get_protector_aura():
			return (self.get_str() * self.get_protector() * 5) + (self.get_protector() * 3 * self.get_dex()) + (self.get_protector() * 90 * self.get_dur() * self.get_prot())

	def get_protector_armor():
			return ((self.get_protector() * 3 * self.get_str()) + (self.get_protector() * 6 * self.get_dex()) + (self.get_protector() * 9 * self.get_dur())) / 1.2

	def get_protector_shield():
			return ((self.get_protector() * 3 * self.get_int) + (self.get_protector() * 6 * self.get_wis()) + (self.get_protector() * 9 * self.get_prot())) / 1.2

	def get_protector_phy():
			return (self.get_protector() * 4 * self.get_str()) + (self.get_protector() * 4 * self.get_dex())

	def get_protector_mag():
			return (self.get_protector() * 4 * self.get_int) + (self.get_protector() * 4 * self.get_wis())

	def get_protector_invest():
			if self._PROTECTOR != 0:
					return self.get_mana() * 2 + self.get_dur() * 2 - self.get_titan() / self.get_level() * self.get_mana() * 1 - self.get_dur() * 1 * self.get_titan() / self.get_level() - self.get_paladin() / self.get_level() * self.get_mana() * 0.5 - self.get_paladin() / self.get_level() * self.get_dur() * 0.5 - self.get_berserk() / self.get_level() * self.get_mana() * 0.5 - self.get_berserk() / self.get_level() * self.get_dur() * 0.5 - self.get_assassin() / self.get_level() * self.get_mana() * 1 - self.get_bandit() / self.get_level() * 0.8
					
			else:
					return 0


	"""
			Mage
	"""


	def get_mage_aura():
			return (self.get_mage() * 3 * self.get_int) + (self.get_mage() * 1 * self.get_dex()) + (self.get_mage() * 60 * self.get_dur() * self.get_prot())

	def get_mage_armor():
			return ((self.get_mage() * 1 * self.get_str()) + (self.get_mage() * 2 * self.get_dex()) + (self.get_mage() * 4 * self.get_dur())) / 1.2

	def get_mage_shield():
			return ((self.get_mage() * 5 * self.get_int) + (self.get_mage() * 4 * self.get_wis()) + (self.get_mage() * 7 * self.get_prot())) / 1.2

	def get_mage_phy():
			return (self.get_mage() * 2 * self.get_str()) + (self.get_mage() * 1 * self.get_dex())

	def get_mage_mag():
			return (self.get_mage() * 6 * self.get_int) + (self.get_mage() * 6 * self.get_wis())

	def get_mage_invest():
			if self._MAGE != 0:
					return self.get_mana() * 5 + self.get_wis() + self.get_int + self.get_sorcerer() * self.get_int * 1.2 - self.get_titan() / self.get_level() * self.get_mana() * 1 - self.get_wis() * 1 * self.get_titan() / self.get_level() - self.get_paladin() / self.get_level() * self.get_mana() * 0.5 - self.get_paladin() / self.get_level() * self.get_wis() * 0.5 - self.get_berserk() / self.get_level() * self.get_mana() * 0.5 - self.get_berserk() / self.get_level() * self.get_wis() * 0.5 - self.get_assassin() / self.get_level() * self.get_mana() * 1 - self.get_bandit() / self.get_level() * 0.8

			else:
					return 0


	"""
			Warrior
	"""


	def get_warrior_aura():
			return (self.get_warrior() * 3 * self.get_str()) + (self.get_warrior() * 1 * self.get_dex()) + (self.get_warrior() * 60 * self.get_dur() * self.get_prot())

	def get_warrior_armor():
			return ((self.get_warrior() * 5 * self.get_str()) + (self.get_warrior() * 4 * self.get_dex()) + (self.get_warrior() * 7 * self.get_dur())) / 1.2

	def get_warrior_shield():
			return ((self.get_warrior() * 1 * self.get_int()) + (self.get_warrior() * 2 * self.get_wis()) + (self.get_warrior() * 4 * self.get_prot())) / 1.2

	def get_warrior_phy():
			return (self.get_warrior() * 6 * self.get_str()) + (self.get_warrior() * 6 * self.get_dex())

	def get_warrior_mag():
			return (self.get_warrior() * 4 * self.get_int()) + (self.get_warrior() * 1 * self.get_wis())

	def get_warrior_invest():
			if self._WARRIOR != 0:
					return self.get_mana() * 5 + self.get_str() * 2 + self.get_mercenary() * self.get_str() * 1.2 - self.get_titan() / self.get_level() * self.get_mana() * 1 - self.get_str() * 1 * self.get_titan() / self.get_level() - self.get_paladin() / self.get_level() * self.get_mana() * 0.5 - self.get_paladin() / self.get_level() * self.get_str() * 0.5 - self.get_titan() / self.get_level() * self.get_mana() * 0.5 - self.get_titan() / self.get_level() * self.get_str() * 0.5 - self.get_assassin() / self.get_level() * self.get_mana() * 1
			else:
					return 0

	"""
			Cleric
	"""


	def get_cleric_aura():
			return (self.get_cleric() * 3 * self.get_str()) + (self.get_cleric() * 1 * self.get_dex()) + (self.get_cleric() * 60 * self.get_dur() * self.get_prot())

	def get_cleric_armor():
			return ((self.get_cleric() * 1 * self.get_str()) + (self.get_cleric() * 2 * self.get_dex()) + (self.get_cleric() * 3 * self.get_dur())) / 1.2

	def get_cleric_shield():
			return ((self.get_cleric() * 4 * self.get_int()) + (self.get_cleric() * 4 * self.get_wis()) + (self.get_cleric() * 5 * self.get_prot())) / 1.2

	def get_cleric_phy():
			return (self.get_cleric() * 7 * self.get_str()) + (self.get_cleric() * 1 * self.get_dex())

	def get_cleric_mag():
			return (self.get_cleric() * 6 * self.get_int()) + (self.get_cleric() * 5 * self.get_wis())

	def get_cleric_invest():
			if self._CLERIC != 0:
					return self.get_mana() * 4 + self.get_cha() * 2 - self.get_titan() / self.get_level() * self.get_mana() * 1 - self.get_str() * 1 * self.get_titan() / self.get_level() - self.get_paladin() / self.get_level() * self.get_mana() * 0.5 - self.get_paladin() / self.get_level() * self.get_str() * 0.5 - self.get_titan() / self.get_level() * self.get_mana() * 0.5 - self.get_titan() / self.get_level() * self.get_str() * 0.5 - self.get_assassin() / self.get_level() * self.get_mana() * 1 - self.get_bandit() / self.get_level() * 0.8
			else:
					return 0


	"""
			Hero
	"""


	def get_hero_aura():
			return (self.get_hero() * 4 * self.get_str()) + (self.get_hero() * 2 * self.get_dex()) + (self.get_hero() * 90 * self.get_dur() * self.get_prot())

	def get_hero_armor():
			return ((self.get_hero() * 4 * self.get_str()) + (self.get_hero() * 4 * self.get_dex()) + (self.get_hero() * 5 * self.get_dur())) / 1.2

	def get_hero_shield():
			return ((self.get_hero() * 4 * self.get_int()) + (self.get_hero() * 4 * self.get_wis()) + (self.get_hero() * 5 * self.get_prot())) / 1.2

	def get_hero_phy():
			return (self.get_hero() * 5 * self.get_str()) + (self.get_hero() * 4 * self.get_dex())

	def get_hero_mag():
			return (self.get_hero() * 5 * self.get_int()) + (self.get_hero() * 4 * self.get_wis())

	def get_hero_invest():
			if self._HERO != 0:
					return self.get_mana() * 1 + self.get_prot() + self.get_wis() + self.get_int() + self.get_cha() + self.get_str() + self.get_dex() + self.get_prot() - self.get_titan() / self.get_level() * self.get_mana() * 1 - self.get_prot() * 1 * self.get_titan() / self.get_level() - self.get_paladin() / self.get_level() * self.get_mana() * 0.5 - self.get_paladin() / self.get_level() * self.get_prot() * 0.5 - self.get_titan() / self.get_level() * self.get_mana() * 0.5 - self.get_titan() / self.get_level() * self.get_prot() * 0.5 - self.get_assassin() / self.get_level() * self.get_mana() * 1 - self.get_bandit() / self.get_level() * 0.8

			else:
					return 0


	"""
			Titan
	"""


	def get_titan_aura():
			return (self.get_titan() * 80 * self.get_str()) + (self.get_titan() * 30 * self.get_dex()) + (self.get_titan() * 110 * self.get_dur() * self.get_prot())

	def get_titan_armor():
			return ((self.get_titan() * 8 * self.get_str()) + (self.get_titan() * 6 * self.get_dex()) + (self.get_titan() * 10 * self.get_dur())) / 1.2

	def get_titan_shield():
			return ((self.get_titan() * 8 * self.get_int()) + (self.get_titan() * 6 * self.get_wis()) + (self.get_titan() * 10 * self.get_prot())) / 1.2

	def get_titan_phy():
			return (self.get_titan() * 2 * self.get_str()) + (self.get_titan() * 2 * self.get_dex())

	def get_titan_mag():
			return (self.get_titan() * 2 * self.get_int()) + (self.get_titan() * 2 * self.get_wis())

	def get_titan_reserve():
			return - self.get_titan() / self.get_level() * self.get_mana() * 20

	def get_titan_detection():
			return self.get_titan() * self.get_wis() * 16


	"""
			Paladin
	"""


	def get_paladin_aura():
			return (self.get_paladin() * 10 * self.get_int()) + (self.get_paladin() * 70 * self.get_dur()) + (self.get_paladin() * 110 * self.get_prot())

	def get_paladin_armor():
			return ((self.get_paladin() * 4 * self.get_str()) + (self.get_paladin() * 5 * self.get_dex()) + (self.get_paladin() * 8 * self.get_dur())) / 1.2

	def get_paladin_shield():
			return ((self.get_paladin() * 6 * self.get_int()) + (self.get_paladin() * 7 * self.get_wis()) + (self.get_paladin() * 8 * self.get_prot())) / 1.2

	def get_paladin_phy():
			return ((self.get_paladin() * 3 * self.get_str()) + (self.get_paladin() * 4 * self.get_dex())) * 1.5

	def get_paladin_mag():
			return ((self.get_paladin() * 5 * self.get_int()) + (self.get_paladin() * 9 * self.get_wis())) * 1.5

	def get_paladin_detection():
			return self.get_paladin() * 10 * self.get_wis()


	"""
			Berserk
	"""


	def get_berserk_aura():
			return (self.get_titan() * 10 * self.get_str()) + (self.get_titan() * 110 * self.get_dur()) + (self.get_titan() * 70 * self.get_prot())

	def get_berserk_armor():
			return ((self.get_titan() * 6 * self.get_str()) + (self.get_titan() * 7 * self.get_dex()) + (self.get_titan() * 8 * self.get_dur())) / 1.2

	def get_berserk_shield():
			return ((self.get_titan() * 4 * self.get_int()) + (self.get_titan() * 5 * self.get_wis()) + (self.get_titan() * 8 * self.get_prot())) / 1.2

	def get_berserk_phy():
			return ((self.get_titan() * 5 * self.get_str()) + (self.get_titan() * 9 * self.get_dex())) * 1.5

	def get_berserk_mag():
			return ((self.get_titan() * 3 * self.get_int()) + (self.get_titan() * 4 * self.get_wis())) * 1.5

	def get_berserk_detection():
			return self.get_titan() * self.get_wis() * 10


	"""
			Wizard
	"""


	def get_wizard_aura():
			return (self.get_paladin() * 30 * self.get_dur() * self.get_prot())

	def get_wizard_armor():
			return ((self.get_paladin() * 3 * self.get_dex()) + (self.get_paladin() * 3 * self.get_dur())) / 1.2

	def get_wizard_shield():
			return ((self.get_paladin() * 3 * self.get_int()) + (self.get_paladin() * 6 * self.get_wis()) + (self.get_paladin() * 6 * self.get_prot())) / 1.2

	def get_wizard_phy():
			return (self.get_paladin() * 2 * self.get_dex())

	def get_wizard_mag():
			return (self.get_paladin() * 6 * self.get_int()) + (self.get_paladin() * 3 * self.get_wis())

	def get_wizard_reserve():
			return self.get_paladin() * 5 * self.get_mana()


	"""
			Sagacious
	"""


	def get_sagacious_aura():
			return (self.get_sagacious() * 60 * self.get_dur() * self.get_prot())

	def get_sagacious_armor():
			return ((self.get_sagacious() * 4 * self.get_dex()) + (self.get_sagacious() * 5 * self.get_dur())) / 1.2

	def get_sagacious_shield():
			return ((self.get_sagacious() * 5 * self.get_int()) + (self.get_sagacious() * 8 * self.get_wis()) + (self.get_sagacious() * 7 * self.get_prot())) / 1.2

	def get_sagacious_phy():
			return (self.get_sagacious() * 3 * self.get_dex())

	def get_sagacious_mag():
			return (self.get_sagacious() * 5 * self.get_int()) + (self.get_sagacious() * 2 * self.get_wis())

	def get_sagacious_reserve():
			return self.get_sagacious() * 5 * self.get_mana()


	"""
			Sorcerer
	"""

	def get_sorcerer_aura():
			return (self.get_sorcerer() * 3 * self.get_dur() * self.get_prot())

	def get_sorcerer_armor():
			return ((self.get_sorcerer() * 2 * self.get_dex()) + (self.get_sorcerer() * 2 * self.get_dur())) / 1.2

	def get_sorcerer_shield():
			return ((self.get_sorcerer() * 2 * self.get_int()) + (self.get_sorcerer() * 4 * self.get_wis()) + (self.get_sorcerer() * 3 * self.get_prot())) / 1.2

	def get_sorcerer_phy():
			return (self.get_sorcerer() * 2 * self.get_dex())

	def get_sorcerer_mag():
			return (self.get_sorcerer() * 9 * self.get_int()) + (self.get_sorcerer() * 3 * self.get_wis())

	def get_sorcerer_reserve():
			return self.get_sorcerer() * 6 * self.get_mana()

	def get_sorcerer_invest():
			return self.get_wis() * 2 * self.get_sorcerer()


	"""
			Knight
	"""


	def get_knight_aura():
			return (self.get_knight() * 4 * self.get_str()) + (self.get_knight() * 30 * self.get_dur() * self.get_prot())

	def get_knight_armor():
			return ((self.get_knight() * 3 * self.get_str()) + (self.get_knight() * 6 * self.get_dex()) + (self.get_knight() * 6 * self.get_dur())) / 1.2

	def get_knight_shield():
			return ((self.get_knight() * 3 * self.get_wis()) + (self.get_knight() * 3 * self.get_prot())) / 1.2

	def get_knight_phy():
			return (self.get_knight() * 6 * self.get_str()) + (self.get_knight() * 3 * self.get_dex())

	def get_knight_mag():
			return (self.get_knight() * 2 * self.get_wis())

	def get_knight_reserve():
			return self.get_knight() * 5 * self.get_mana()


	"""
			Defender
	"""


	def get_defender_aura():
			return (self.get_defender() * 6 * self.get_str()) + (self.get_defender() * 60 * self.get_dur() * self.get_prot())

	def get_defender_armor():
			return ((self.get_defender() * 5 * self.get_str()) + (self.get_defender() * 8 * self.get_dex()) + (self.get_defender() * 7 * self.get_dur())) / 1.2

	def get_defender_shield():
			return ((self.get_defender() * 4 * self.get_wis()) + (self.get_defender() * 5 * self.get_prot())) / 1.2

	def get_defender_phy():
			return (self.get_defender() * 5 * self.get_str()) + (self.get_defender() * 2 * self.get_dex())

	def get_defender_mag():
			return (self.get_defender() * 3 * self.get_wis())

	def get_defender_reserve():
			return self.get_defender() * 5 * self.get_mana()


	"""
			Mercenary
	"""


	def get_mercenary_aura():
			return (self.get_mercenary() * 0.5 * self.get_str()) + (self.get_mercenary() * 3 * self.get_dur() * self.get_prot())

	def get_mercenary_armor():
			return ((self.get_mercenary() * 2 * self.get_str()) + (self.get_mercenary() * 4 * self.get_dex()) + (self.get_mercenary() * 3 * self.get_dur())) / 1.2

	def get_mercenary_shield():
			return ((self.get_mercenary() * 2 * self.get_wis()) + (self.get_sorcerer() * 2 * self.get_prot())) / 1.2

	def get_mercenary_phy():
			return (self.get_mercenary() * 9 * self.get_str()) + (self.get_mercenary() * 3 * self.get_dex())

	def get_mercenary_mag():
			return (self.get_mercenary() * 2 * self.get_wis())

	def get_mercenary_reserve():
			return self.get_mercenary() * 6 * self.get_mana()

	def get_mercenary_invest():
			return self.get_dex() * 0.75 * self.get_mercenary()


	"""
			Priest
	"""


	def get_priest_aura():
			return (self.get_priest() * 30 * self.get_dur() * self.get_prot())

	def get_priest_armor():
			return (self.get_priest() * 4 * self.get_dur() / 1.2)

	def get_priest_shield():
			return ((self.get_priest() * 4 * self.get_prot())) / 1.2

	def get_priest_phy():
			return (self.get_priest() * 4 * self.get_dex())

	def get_priest_mag():
			return (self.get_priest() * 5 * self.get_wis())

	def get_priest_reserve():
			return self.get_priest() * 10 * self.get_mana()


	"""
			Assassin
	"""

	def get_assassin_aura():
			return (self.get_assassin() * 4 * self.get_dex()) + (self.get_assassin() * 30 * self.get_dur() + self.get_assassin() * 20 * self.get_prot())

	def get_assasssin_armor():
			return ((self.get_assassin() * 3 * self.get_str()) + (self.get_assassin() * 6 * self.get_dex()) + (self.get_assassin() * 2 * self.get_dur())) / 1.2

	def get_assassin_shield():
			return ((self.get_assassin() * 3 * self.get_int()) + (self.get_assassin() * 6 * self.get_wis()) + (self.get_assassin() * 2 * self.get_prot())) / 1.2

	def get_assassin_phy():
			return (self.get_assassin() * 5 * self.get_str()) + (self.get_assassin() * 4 * self.get_dex())

	def get_assassin_mag():
			return (self.get_assassin() * 5 * self.get_int()) + (self.get_assassin() * 4 * self.get_wis())

	def get_assassin_detection():
			return self.get_int() * 7 * self.get_assassin() + 12 * self.get_wis() * self.get_assassin()

	def get_assassin_control():
			return self.get_assassin() * self.get_dex() * 15


	"""
			Bandit
	"""

	def get_bandit_aura():
			return (self.get_bandit() * 4 * self.get_dex()) + (self.get_bandit() * 40 * self.get_dur() + self.get_bandit() * 30 * self.get_prot())

	def get_bandit_armor():
			return ((self.get_bandit() * 5 * self.get_str()) + (self.get_bandit() * 7 * self.get_dex()) + (self.get_bandit() * 4 * self.get_dur())) / 1.2

	def get_bandit_shield():
			return ((self.get_bandit() * 5 * self.get_int()) + (self.get_bandit() * 7 * self.get_wis()) + (self.get_bandit() * 4 * self.get_prot())) / 1.2

	def get_bandit_phy():
			return (self.get_bandit() * 7 * self.get_str()) + (self.get_bandit() * 5 * self.get_dex())

	def get_bandit_mag():
			return (self.get_bandit() * 7 * self.get_int()) + (self.get_bandit() * 5 * self.get_wis())

	def get_bandit_detection():
			return 5 * self.get_int() * self.get_bandit() + 9 * self.get_wis() * self.get_bandit()

	def get_bandit_control():
			return self.get_bandit() * self.get_dex() * 10


	"""
			All
	"""


	def get_all_reserve():
			return self.get_mana() * 35

	def get_all_detection():
			return self.get_wis() * 20 + self.get_cha() * 10 + self.get_int() * 5

	def get_all_control():
			return self.get_dex() * 20 + self.get_str() * 5 + self.get_cha() * 10


	"""
			Main stats

			NOTE: These should be left untouched UNLESS there is a new class. Touch the original aura calculators instead!
	"""


	def get_aura():
			return int((self.get_protector_aura() + self.get_mage_aura() + self.get_warrior_aura() + self.get_cleric_aura() + self.get_hero_aura() + self.get_titan_aura() + self.get_paladin_aura() + self.get_berserk_aura() + self.get_wizard_aura() + self.get_sagacious_aura() + self.get_sorcerer_aura() + self.get_knight_aura() + self.get_defender_aura() + self.get_mercenary_aura() + self.get_priest_aura() + self.get_assassin_aura() + self.get_bandit_aura()) / 15)

	def get_willpower():
			return get_aura() // 4

	def get_armor():
			return int(self.get_protector_armor() + self.get_mage_armor() + self.get_warrior_armor() + self.get_cleric_armor() + self.get_hero_armor() + self.get_titan_armor() + self.get_paladin_armor() + self.get_berserk_armor() + self.get_wizard_armor() + self.get_sagacious_armor() + self.get_sorcerer_armor() + self.get_knight_armor() + self.get_defender_armor() + self.get_mercenary_armor() + self.get_priest_armor() + self.get_assassin_armor() + self.get_bandit_armor())

	def get_shield():
			return int(self.get_protector_shield() + self.get_mage_shield() + self.get_warrior_shield() + self.get_cleric_shield() + self.get_hero_shield() + self.get_titan_shield() + self.get_paladin_shield() + self.get_berserk_shield() + self.get_wizard_shield() + self.get_sagacious_shield() + self.get_sorcerer_shield() + self.get_knight_shield() + self.get_defender_shield() + self.get_mercenary_shield() + self.get_priest_shield() + self.get_assassin_shield() + self.get_bandit_shield())

	def get_phy():
			return int(self.get_protector_phy() + self.get_mage_phy() + self.get_warrior_phy() + self.get_cleric_phy() + self.get_hero_phy() + self.get_titan_phy() + self.get_paladin_phy() + self.get_berserk_phy() + self.get_wizard_phy() + self.get_sagacious_phy() + self.get_sorcerer_phy() + self.get_knight_phy() + self.get_defender_phy() + self.get_mercenary_phy() + self.get_priest_phy() + self.get_assassin_phy() + self.get_bandit_phy())

	def get_mag():
			return int(self.get_protector_mag() + self.get_mage_mag() + self.get_warrior_mag() + self.get_cleric_mag() + self.get_hero_mag() + self.get_titan_mag() + self.get_paladin_mag() + self.get_berserk_mag() + self.get_wizard_mag() + self.get_sagacious_mag() + self.get_sorcerer_mag() + self.get_knight_mag() + self.get_defender_mag() + self.get_mercenary_mag() + self.get_priest_mag() + self.get_assassin_mag() + self.get_bandit_mag())

	def get_reserve():
			return int(self.get_protector_reserve() + self.get_mage_reserve() + self.get_warrior_reserve() + self.get_cleric_reserve() + self.get_hero_reserve() + self.get_titan_reserve() + self.get_paladin_reserve() + self.get_berserk_reserve() + self.get_wizard_reserve() + self.get_sagacious_reserve() + self.get_sorcerer_reserve() + self.get_knight_reserve() + self.get_defender_reserve() + self.get_mercenary_reserve() + self.get_priest_reserve() + self.get_assassin_reserve() + self.get_bandit_reserve() + self.get_all_reserve())

	def get_detection():
			return int(self.get_protector_detection() + self.get_mage_detection() + self.get_warrior_detection() + self.get_cleric_detection() + self.get_hero_detection() + self.get_titan_detection() + self.get_paladin_detection() + self.get_berserk_detection() + self.get_wizard_detection() + self.get_sagacious_detection() + self.get_sorcerer_detection() + self.get_knight_detection() + self.get_defender_detection() + self.get_mercenary_detection() + self.get_priest_detection() + self.get_assassin_detection() + self.get_bandit_detection() + self.get_all_detection())

	def get_control():
			return int(self.get_protector_control() + self.get_mage_control() + self.get_warrior_control() + self.get_cleric_control() + self.get_hero_control() + self.get_titan_control() + self.get_paladin_control() + self.get_berserk_control() + self.get_wizard_control() + self.get_sagacious_control() + self.get_sorcerer_control() + self.get_knight_control() + self.get_defender_control() + self.get_mercenary_control() + self.get_priest_control() + self.get_assassin_control() + self.get_bandit_control() + self.get_all_control())

	def get_recovery():
			return int((self.get_reserve() / 4 + self.get_wis() * 10) + self.buffed['recovery'] + (self.get_reserve() / 4 + self.get_wis() * 10) * self.buffed['recovery'] / 100)

	def get_invest():
			return int(self.get_protector_invest() + self.get_mage_invest() + self.get_warrior_invest() + self.get_clerif_invest() + self.get_her_invest() + self.get_sorcerer_invest() + self.get_mercenary_invest())


	"""
			Studies
	"""


	def get_study_body(modifier: int = 1) -> int:
			return int((self.get_str() + (self.get_str() * (self.buffed['_body'][1] / 100))) * modifier / 10 + self.static_buffed['_body'][1]) + (self.get_berserk() * (self.get_str() * 2)) + (self.get_knight() * (self.get_str() * 6)) + (self.get_defender() * (self.get_str() * 2) - (self.get_prot() * 10 * (self.get_defender()))) + (self.get_mercenary() * (self.get_str() * 40))

	def get_study_transformation(modifier: int = 1) -> int:
			return int((self.get_dex() + (self.get_dex() * (self.buffed['_transformation'][1] / 100))) * modifier / 10 + self.static_buffed['_transformation'][1]) + (self.get_assassin() * (self.get_dex() * 60)) + (self.get_knight() * (self.get_dex() * 6)) + (self.get_mercenary() * (self.get_dex() * 40)) + (self.get_bandit() * (self.get_dex() * 20))

	def get_study_creation(modifier: int = 1) -> int:
			return int((self.get_wis() + (self.get_wis() * (self.buffed['_creation'][1] / 100))) * modifier / 10 + self.static_buffed['_creation'][1]) + (self.get_wizard() * (self.get_wis() * 20)) + (self.get_sagacious() * (self.get_wis() * 10)) + (self.get_sorcerer() * (self.get_wis() * 40))

	def get_study_aura(modifier: int = 1) -> int:
			return int((((self.get_dur() + self.get_prot()) / 2 + ((self.get_dur() / 2 + self.get_prot() / 2) * (self.buffed['_aura'][1] / 100))) * modifier / 10 + self.static_buffed['_aura'][1]) / 1.5) + (O7 * ((self.get_dur() * 11) + (self.get_prot() * 11))) + (self.get_defender() * ((self.get_dur() * 6) + (self.get_prot() * 6)))

	def get_study_destruction(modifier: int = 1) -> int:
			return int((self.get_int() + (self.get_int() * (self.buffed['_destruction'][1] / 100))) * modifier / 10 + self.static_buffed['_destruction'][1]) + (self.get_wizard() * (self.get_int() * 20)) + (self.get_sagacious() * (self.get_int() * 10)) + (self.get_sorcerer() * (self.get_int() * 40))

	def get_study_life(modifier: int = 1) -> int:
			return int((self.get_cha() + (self.get_cha() * (self.buffed['_life'][1] / 100))) * modifier / 10 + self.static_buffed['_life'][1]) + (self.get_sagacious() * (self.get_cha() * 20)) + (self.get_priest() * (self.get_cha() * 80))


	"""
			Miscellaneous
	"""


	def get_level(self) -> int:
			"""Magic exp -> level calculation.

			https: // www.desmos.com / calculator / epwt1dkahp"""

			return math.floor(self._EXP ** (1 / 4) + self._EXP ** (1 / 3) + 1)


	def get_movement(self):
			movement = 2

			if self._TITAN != 0:
					movement -= 1

			elif self._ASSASSIN != 0:
					movement += 1

			elif self._BANDIT != 0:
					movement += 2

			return movement


	"""
			SQL
	"""


	@staticmethod
	def db_load_from_id(userid):
			"""
			Loads a user from the db and returns a Player object

			:param: `userid`: User's unique Discord ID

			Returns new Player Object
			"""

			connection = connect('dbname=userdat.db user=postgres')
			cursor = connection.cursor()

			cursor.execute('SELECT * FROM playerdat WHERE USERID=%s LIMIT 1;', (userid,))

			player_Object = Player(*cursor.fetchone())

			cursor.close()
			connection.close()

			return player_Object


	@staticmethod
	def db_get_init(userid) -> int:
			"""
			Returns a player's initiative from their id

			:param: `userid`: User's unique Discord ID
			"""

			connection = connect('dbname=userdat.db user=postgres')
			cursor = connection.cursor()

			cursor.execute('SELECT DEX FROM playerdat WHERE USERID=%s;', (userid,))

			init = cursor.fetchone() // 50

			cursor.close()
			connection.close()

			return init


	def db_full_update(self):
			"""Does a full update on all possibly altered items. Do :meth:`db_sheet_update()` for updating absolutely everything."""

			connection = connect('dbname=userdat.db user=postgres')
			cursor = connection.cursor()

			cursor.execute('UPDATE playerdat SET EXP=%s, RANK=%s,' + 
									'PROTECTOR=%s, MAGE=%s, WARRIOR=%s, CLERIC=%s, HERO=%s, TITAN=%s, PALADIN=%s, BERSERK=%s, WIZARD=%s, SAGACIOUS=%s, SORCERER=%s, KNIGHT=%s, DEFENDER=%s, MERCENARY=%s, PRIEST=%s, ASSASSIN=%s, BANDIT=%s,' + 
									'STR=%s, DEX=%s, DUR=%s, INTEL=%s, CHA=%s, WIS=%s, PROT=%s, MANA=%s WHERE USERID=%s LIMIT 1;',
											(self._EXP, self._RANK,
											self._PROTECTOR, self._MAGE, self._WARRIOR, self._CLERIC, self._HERO,
											self._TITAN, self._PALADIN, self._BERSERK, self._WIZARD, self._SAGACIOUS, self._SORCEROR, self._KNIGHT, self._DEFENDER, self._MERCENARY, self._PRIEST, self._ASSASSIN, self._BANDIT,
											self._STR, self._DEX, self._DUR, self._INT, self._CHA, self._WIS, self._PROT, self._MANA,
											self._USERID))

			connection.commit()

			cursor.close()
			connection.close()


	def db_stat_update(self):
			"""Updates all of the stats in the db."""

			connection = connect('dbname=userdat.db user=postgres')
			cursor = connection.cursor()

			cursor.execute('UPDATE playerdat SET STR=%s, DEX=%s, DUR=%s, INTEL=%s, CHA=%s, WIS=%s, PROT=%s, MANA=%s WHERE USERID=%s LIMIT 1;',
					(self._STR, self._DEX, self._DUR, self._INT, self._CHA, self._PROT, self._MANA, self._USERID))

			connection.commit()

			cursor.close()
			connection.close()


	def db_sheet_update(self):
			"""
			Updates absolutely everything and checks if there is a new name and it is not already taken. 
			Doesn't actually update the sheet, but gets all of the data ready in the weird case we may need this.
			"""

			connection = connect('dbname=userdat.db user=postgres')
			cursor = connection.cursor()

			cursor.execute('SELECT NAME FROM playerdat WHERE USERID=%s;', self._USERID)

			name = cursor.fetchone()

			if self._NAME != name:
					cursor.execute('SELECT 1 FROM playerdat WHERE NAME=%s;', self._NAME)

					if cursor.fetchone() == 1:
							raise NameException('Name is already taken.')

			connection.execute('UPDATE playerdat SET NAME=%s, EXP=%s, RANK=%s, AGE=%s, BIRTHDAY=%s, HEIGHT=%s, YEAR=%s, GENDER=%s, AURA=%s, IMAGE1=%s, IMAGE2=%s, THEME=%s, INFO=%s, BACKSTORY=%s' + 
									'PROTECTOR=%s, MAGE=%s, WARRIOR=%s, CLERIC=%s, HERO=%s, TITAN=%s, PALADIN=%s, BERSERK=%s, WIZARD=%s, SAGACIOUS=%s, SORCERER=%s, KNIGHT=%s, DEFENDER=%s, MERCENARY=%s, PRIEST=%s, ASSASSIN=%s, BANDIT=%s,' + 
									'STR=%s, DEX=%s, DUR=%s, INTEL=%s, CHA=%s, WIS=%s, PROT=%s, MANA=%s, CURRENTAURA=%s, CURRENTHEALTH=%s, CURRENTMANA=%s, WHERE USERID=%s LIMIT 1;',
											(self._NAME, self._EXP, self._RANK, self._AGE, self._BIRTHDAY, self._HEIGHT, self._YEAR, self._GENDER, self._AURA, self._IMAGE1, self._IMAGE2, self._THEME, self._INFO, self._BACKSTORY,
											self._PROTECTOR, self._MAGE, self._WARRIOR, self._CLERIC, self._HERO,
											self._TITAN, self._PALADIN, self._BERSERK, self._WIZARD, self._SAGACIOUS, self._SORCEROR, self._KNIGHT, self._DEFENDER, self._MERCENARY, self._PRIEST, self._ASSASSIN, self._BANDIT,
											self._STR, self._DEX, self._DUR, self._INT, self._CHA, self._WIS, self._PROT, self._MANA,
											self._CURRENTAURA, self._CURRENTHEALTH, self._CURRENTMANA,
											self._USERID))

			connection.commit()

			cursor.close()
			connection.close()
