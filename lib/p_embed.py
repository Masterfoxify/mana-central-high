def init(ctx, userid):
	aura_color = discord.Colour(0x94c9d3)
	info_embed = discord.Embed(title="Character Name (Lv. 1)", colour=aura_color,  description="**Appearance**\n*Description here...*\n\n**Personality**\n*Description here...*\n\n**Backstory**\n*Description here...*") # creation date

	info_embed.set_image(url="https://cdn.discordapp.com/embed/avatars/0.png") # character image
	info_embed.set_author(name="Player's Discord Name", icon_url="https://cdn.discordapp.com/embed/avatars/3.png") # player dicord icon
	info_embed.set_footer(text="Mana Central High", icon_url="https://cdn.discordapp.com/embed/avatars/4.png") # mana central high logo

	info_embed.add_field(name="Experience", value="0", inline=True)
	info_embed.add_field(name="Experience to Next Level", value="1", inline=True)
	info_embed.add_field(name="Gender", value="mystery", inline=True)
	info_embed.add_field(name="Age", value="0", inline=True)
	info_embed.add_field(name="Height", value="0cm", inline=True)
	info_embed.add_field(name="Aura Color", value="blue??lol", inline=True)
	info_embed.add_field(name="Birthday", value="0 o'clock tuesday", inline=True)
	info_embed.add_field(name="Theme", value="funky", inline=True)

	# statistics

	stat_embed = discord.Embed(colour=aura_color, description="__**Statistics for Character Name**__")

	stat_embed.set_author(name="Player's Discord Name", icon_url="https://cdn.discordapp.com/embed/avatars/3.png")
	stat_embed.set_footer(text="Mana Central High", icon_url="https://cdn.discordapp.com/embed/avatars/4.png")

	stat_embed.add_field(name="Aura", value="0", inline=True)
	stat_embed.add_field(name="Mana Reserve", value="1", inline=True)
	stat_embed.add_field(name="Health", value="0", inline=True)
	stat_embed.add_field(name="Mana Detect.", value="0", inline=True)
	stat_embed.add_field(name="Armor", value="0", inline=True)
	stat_embed.add_field(name="Mana Control", value="0", inline=True)
	stat_embed.add_field(name="Shield", value="0", inline=True)
	stat_embed.add_field(name="Mana Recovery", value="0", inline=True)
	stat_embed.add_field(name="Physical Dmg.", value="0", inline=True)
	stat_embed.add_field(name="Mana Invest", value="0", inline=True)
	stat_embed.add_field(name="Magic Dmg.", value="0", inline=True)
	stat_embed.add_field(name="Movement", value="0", inline=True)

	#skills

	skill_embed = discord.Embed(colour=aura_color, description="__**Skills for Character Name**__")

	skill_embed.set_author(name="Player's Discord Name", icon_url="https://cdn.discordapp.com/embed/avatars/3.png")
	skill_embed.set_footer(text="Mana Central High", icon_url="https://cdn.discordapp.com/embed/avatars/4.png")

	skill_embed.add_field(name="__Minimum__", value="10")
	skill_embed.add_field(name="__Maximum__", value="10")
	skill_embed.add_field(name="__Total__", value="10")
	skill_embed.add_field(name="Strength", value="10", inline=True)
	skill_embed.add_field(name="Intelligence", value="10", inline=True)
	skill_embed.add_field(name="Dexterity", value="10", inline=True)
	skill_embed.add_field(name="Wisdom", value="10", inline=True)
	skill_embed.add_field(name="Durability", value="10", inline=True)
	skill_embed.add_field(name="Charisma", value="10", inline=True)
	skill_embed.add_field(name="Mana", value="10", inline=True)
	skill_embed.add_field(name="Protection", value="10", inline=True)

	# studies

	study_embed = discord.Embed(colour=aura_color, description="__**Studies for Character Name**__")

	study_embed.set_author(name="Player's Discord Name", icon_url="https://cdn.discordapp.com/embed/avatars/3.png")
	study_embed.set_footer(text="Mana Central High", icon_url="https://cdn.discordapp.com/embed/avatars/4.png")

	study_embed.add_field(name="__Physical__", value="_ _", inline=True)
	study_embed.add_field(name="__Magic__", value="_ _", inline=True)
	study_embed.add_field(name="Body", value="0", inline=True)
	study_embed.add_field(name="Aura", value="-1", inline=True)
	study_embed.add_field(name="Transformation", value="100009", inline=True)
	study_embed.add_field(name="Destruction", value="65134", inline=True)
	study_embed.add_field(name="Creation", value="7417", inline=True)
	study_embed.add_field(name="Life", value="65436", inline=True)

	# classes

	class_embed = discord.Embed(colour=aura_color, description="__**Classes for Character Name**__")

	class_embed.set_author(name="Player's Discord Name", icon_url="https://cdn.discordapp.com/embed/avatars/3.png")
	class_embed.set_footer(text="Mana Central High", icon_url="https://cdn.discordapp.com/embed/avatars/4.png")

	class_embed.add_field(name="__Main Classes__", value="_ _")
	class_embed.add_field(name="Hero", value="0", inline=True)
	class_embed.add_field(name="Warrior", value="0", inline=True)
	class_embed.add_field(name="Protector", value="0", inline=True)
	class_embed.add_field(name="Mage", value="0", inline=True)
	class_embed.add_field(name="Cleric", value="0", inline=True)
	class_embed.add_field(name="_ _", value="_ _")
	class_embed.add_field(name="__Subclasses__", value="_ _")
	class_embed.add_field(name="Titan", value="0", inline=True)
	class_embed.add_field(name="Sorcerer", value="0", inline=True)
	class_embed.add_field(name="Paladin", value="0", inline=True)
	class_embed.add_field(name="Knight", value="0", inline=True)
	class_embed.add_field(name="Berserk", value="0", inline=True)
	class_embed.add_field(name="Defender", value="0", inline=True)
	class_embed.add_field(name="Wizard", value="0", inline=True)
	class_embed.add_field(name="Mercenary", value="0", inline=True)
	class_embed.add_field(name="Sagacious", value="0", inline=True)
	class_embed.add_field(name="Priest", value="0", inline=True)
	class_embed.add_field(name="Assassin", value="0", inline=True)
	class_embed.add_field(name="Bandit", value="0", inline=True)

def ability_embed(userid, ability_name):
	# ability

	embed = discord.Embed(colour=discord.Colour(0x94c9d3), description="__**Abilities for Charecter Name**__")

	embed.set_author(name="Player's Discord Name", icon_url="https://cdn.discordapp.com/embed/avatars/3.png")
	embed.set_footer(text="Mana Central High", icon_url="https://cdn.discordapp.com/embed/avatars/4.png")

	embed.add_field(name="__Ability Name__", value="_ _", inline=True)
	embed.add_field(name="Action Type", value="Main", inline=True)
	embed.add_field(name="Study", value="1", inline=True)
	embed.add_field(name="Cost", value="1", inline=True)
	embed.add_field(name="Attack Type", value="Physcal", inline=True)
	embed.add_field(name="Range", value="1", inline=True)
	embed.add_field(name="__Effect__", value="*big bad description*")
	embed.add_field(name="_ _", value="_ _")

	return embed