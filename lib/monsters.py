from players import Player

class Monster(Player):
	"""
	Monster class. Extends :class:`Player` for basic functionality.
	"""

	def __init__(self, *stats, initiative=0):
		self.MONSTERID = stats[0]
		self.NAME = stats[1]
		self.THEME = stats[2]

		self._PROTECTOR = stats[3]
		self._MAGE = stats[4]
		self._WARRIOR = stats[5]
		self._CLERIC = stats[6]
		self._HERO = stats[7]

		self._TITAN = stats[8]
		self._PALADIN = stats[9]
		self._BERSERK = stats[10]
		self._WIZARD = stats[11]
		self._SAGACIOUS = stats[12]
		self._SORCERER = stats[13]
		self._KNIGHT = stats[14]
		self._DEFENDER = stats[15]
		self._MERCENARY = stats[16]
		self._PRIEST = stats[17]
		self._ASSASSIN = stats[18]
		self._BANDIT = stats[19]
		self._BASIC = 0

		self._STR = stats[20]
		self._DEX = stats[21]
		self._DUR = stats[22]
		self._INT = stats[23]
		self._CHA = stats[24]
		self._WIS = stats[25]
		self._PROT = stats[26]
		self._MANA = stats[27]

		self._ABILITIES = dict(stats[28])

		self._AI = stats[29]

		self._CURRENTAURA = self.get_aura()
		self._CURRENTHEALTH = self.get_willpower()
		self._CURRENTMANA = self.get_mana()

		self.buffed = {'aura': [], 'armor': [], 'shield': [], 'phy': [], 'mag': [], 'reserve': [], 'detection': [], 'control': [], 'recovery': [], 'invest': [],
					   'str': [], 'dex': [], 'dur': [], 'int': [], 'cha': [], 'wis': [], 'prot': [], 'mana': [],
					   '_body': [], '_transformation': [], '_aura': [], '_creation': [], '_destruction': [], '_life': []}

		self.static_buffed = {'aura': [], 'armor': [], 'shield': [], 'phy': [], 'mag': [], 'reserve': [], 'detection': [], 'control': [], 'recovery': [], 'invest': [],
							  'str': [], 'dex': [], 'dur': [], 'int': [], 'cha': [], 'wis': [], 'prot': [], 'mana': [],
							  '_body': [], '_transformation': [], '_aura': [], '_creation': [], '_destruction': [], '_life': [],

							  'protector': [], 'mage': [], 'warrior': [], 'cleric': [], 'hero': [],

							  'titan': [], 'paladin': [], 'berserk': [], 'wizard': [], 'sagacious': [], 'sorcerer': [], 'knight': [], 'defender': [],
							  'mercenary': [], 'priest': [], 'assassin': [], 'bandit': [], 'basic': []}

		self.bonuses = {'roll': [], 'dodge': []}

		self.initiative = initiative

	@staticmethod
	def db_load_from_id(monsterid):
		"""
		Loads a user from the db and returns a Player object

		:param: `monster`: Monster's table ID

		Returns new :class:`Monster` Object
		"""

		connection = connect('userdat.db')
		cursor = connection.cursor()

		cursor.execute('SELECT * FROM monsterdat WHERE MONSTERID=? LIMIT 1', userid)

		monster_Object = Player(*cursor.fetchone())

		cursor.close()
		connection.close()

		return monster_Object


	def get_init() -> int:
		"""
		Returns a monster's initiative
		"""

		return self.get_dex() // 50


	def db_get_init(userid) -> int:
		raise UnimplementedError()


	def db_full_update(self):
		raise UnimplementedError()


	def db_stat_update(self):
		raise UnimplementedError()


	def db_sheet_update(self):
		raise UnimplementedError()