# Discord imports
import discord
from discord.ext import commands

# Built-in imports
from sys import path
from datetime import datetime
from json import dumps

path.append('../')

# In-house imports
import main
from lib import botlogger as log

# Dependency imports
from psycopg2 import connect

logging_admin = log.Logger(log.Configuration('admin'))

class Admin:
	def __init__(self, bot):
		self._bot = bot


	"""
		Admin link commands
	"""


	@commands.group(name="link", help='- Command group for Admins. !help link for commands.')
	@commands.has_role("Admin")
	async def link_group(self, ctx):
		pass


	@link_group.command(name="event", invoke_without_command=True)
	@commands.has_role("Admin")
	async def event_rank_message(self, ctx, message_id: str, role_name: str, emoji: str):
		"""- Links an event message to the bot, as well as a role and emoji.
		Syntax is: !link event {message ID} {role_name} {emoji}
		"""

		local_cache = await main.get_cache()

		logging_admin.send(f'Cache pulled: {local_cache}')

		local_cache[message_id] = {}
		local_cache[message_id][emoji] = role_name

		logging_admin.send(f'Local cache key {message_id} successfully added. Role {role_name} has been linked.')
		await ctx.send(f'Local cache key **{message_id}** successfully added. Role **{role_name}** has been linked.')
		
		await main.update_cache(local_cache)


	@link_group.command(name="multi")
	@commands.has_role("Admin")
	async def multi_event_rank_message(self, ctx, *, arg: str):
		"""- Links an event message to the bot, allowing it to recognize it.
		Syntax is: !link event {message ID} (role_name, emoji) (role_name2, emoji2) ...
		"""
		
		message_id = arg.split(' ')[0]

		args = arg.split(' (')[1:]

		local_cache = await main.get_cache()

		logging_admin.send(f'Cache pulled: {local_cache}')

		local_cache[message_id] = {}
		output_str = ''

		# Essentially doing the same as the normal event, just multiple times
		for i in args:
			i_split = i.replace('(', '').replace(')', '').strip().split(' ')
			j = (i_split[0].strip(), i_split[1].strip())

			role_name = j[0]
			emoji = j[1]
			
			local_cache[message_id][emoji] = role_name
			logging_admin.send(f'Role {role_name} successfully added to cache key {message_id}')

			output_str += f'Role {role_name} successfully added to cache key {message_id}\n'

		await ctx.send(output_str)

		await main.update_cache(local_cache)


	@link_group.command(name="auto")
	@commands.has_role("Admin")
	async def auto_multi_event_rank_message(self, ctx, *, arg: str):
		"""- Automatically links an event message to the bot. Same syntax as multi_event_rank_message (or !link multi)
		NOTE: Next message will be the linked message.
		"""

		msg = await self._bot.wait_for('message', check=lambda msg: msg.author == ctx.author)
		await ctx.invoke(self.multi_event_rank_message, arg=f'{msg.id} {arg}')


	@commands.command(name="unlink")
	@commands.has_role("Admin")
	async def unlink_event(self, ctx, *args):
		"""- Unlinks an event from the reaction_cache.
		Syntax: !unlink {message_id1} {message_id2} ...
		"""

		local_cache = await main.get_cache()

		logging_admin.send(f'Cache pulled: {local_cache}')

		output_str = ''

		for i in args:
			del local_cache[i]
			logging_admin.send(f'{i} successfully removed from cache.')
			output_str += f'{i} successfully removed from cache.'

		await ctx.send(output_str)

		await main.update_cache(cache=local_cache)


	"""
		Moderator commands
	"""


	@commands.group(name="mod", description='Command group for Moderators. !help mod for commands.')
	@commands.has_role("Moderator")
	async def mod_group(self, ctx):
		pass


	@mod_group.group(name="warn", invoke_without_command=True)
	@commands.has_role("Moderator")
	async def warn(self, ctx, *, arg):
		"""- Warns a user with a given message.
		Syntax: !mod warn {user}, {message}
		"""

		warned_user = ctx.message.mentions[0]
		warning_message = (' ').join(arg.split(' ')[1:])

		connection = connect('dbname=userdat.db user=postgres')
		cursor = connection.cursor()

		cursor.execute('SELECT WARNINGS FROM userdat WHERE USERID=%s;', (warned_user.id,))
		
		warnings = cursor.fetchone()
		warnings = warnings is not None and warnings[0] or None

		none_bool = warnings is not None
		warnings = none_bool and warnings or ''

		warning = f'\n\n{datetime.now()}\n' + warning_message
		warnings += warning

		if none_bool:
			cursor.execute('UPDATE userdat SET WARNINGS=%s WHERE USERID=%s;', (warnings, warned_user.id))

		else:
			cursor.execute('INSERT INTO userdat (USERID, RANKEXP, WARNINGS) VALUES (%s,%s,%s);',
				(warned_user.id, 0, f'{datetime.now()}\n{warning_message}'))

		connection.commit()
		connection.close()

		divider = '-' * 30
		logging_admin.send(f'\n{divider}\n{warned_user.name} warned for:\n{warning}\n{divider}\n')

		await ctx.send(f'**{warned_user.name}** warned for:{warning}')


	@warn.command(name="get")
	@commands.has_role("Moderator")
	async def get(self, ctx, *, warned_user: discord.User):
		"""- Gets user's warnings.
		"""

		connection = connect('dbname=userdat.db user=postgres')
		cursor = connection.cursor()
		cursor.execute('SELECT WARNINGS FROM userdat WHERE USERID=%s;', (warned_user.id,))
		
		warnings = cursor.fetchone()
		warnings = warnings is not None and warnings[0] or 'User has no warnings.'

		logging_admin.send(f'Warnings pulled for user {warned_user.name} by {ctx.message.author.name} at {datetime.now()}')

		connection.close()

		channel = ctx.author.dm_channel
		channel = channel is not None and channel or await ctx.author.create_dm()

		await channel.send( f'__**{warned_user.name}\'s Warnings:**__\n\n{warnings}')


	@warn.command(name="dismiss")
	@commands.has_role("Moderator")
	async def dismiss(self, ctx, *, warned_user: discord.User):
		"""- Allows one to dismiss a user's warnings.
		Syntax: !mod warn dismiss ...
		(bot sends warnings)
		1, 7, 3
		NOTE: Next message will be taken as input.
		"""

		connection = connect('dbname=userdat.db user=postgres')
		cursor = connection.cursor()
		cursor.execute('SELECT WARNINGS FROM userdat WHERE USERID=%s;', (warned_user.id,))
		
		warnings = cursor.fetchone()
		user_check = warnings is not None
		warnings = warnings is not None and warnings[0] or warnings

		if user_check:

			logging_admin.send(f'Warning dismissal for {warned_user.name} initiated by {ctx.message.author}.')

			warning_message = warnings.split('\n\n')

			channel = ctx.author.dm_channel
			channel = channel is not None and channel or await ctx.author.create_dm()

			await channel.send(f'__**{warned_user.name}\'s Warnings:**__')

			for warning in enumerate(warning_message):
				await channel.send(f'{warning[0] + 1}:\n{warning[1]}')

			msg = await self._bot.wait_for('message', check=lambda msg: msg.author == ctx.author)

			if msg in ['cancel', 'exit', 'stop']:
				await channel.send(f'Cancelling warning dismissal for **{warned_user[name]}**.')
				logging_admin.send(f'Warning dismissal for {warned_user.name} cancelled by {ctx.message.author}.')

			indices = [x.strip() for x in msg.content.split(',')]

			for j in indices:
				k = int(j) - 1

				divider = '-' * 30
				logging_admin.send(f'\nWARNING DISMISSED:\n{divider}\n{warning_message[k]}\n{divider}\n')
				warning_message[k] = ''

			output_warnings = []
			for j in warning_message:
				if not j == '':
					output_warnings.append(j)

			warnings = ('\n\n').join(output_warnings)

			await channel.send('Warnings(s) successfully dismissed.')

			cursor.execute('UPDATE userdat SET WARNINGS=%s WHERE USERID=%s;', (warnings, warned_user.id))
			connection.commit()

		else:
			await channel.send('Error: User has no warnings to dismiss.')

		logging_admin.send(f'Warning dismissal for {warned_user.name} completed by {ctx.message.author}.')

		connection.close()


def setup(bot):
	bot.add_cog(Admin(bot))