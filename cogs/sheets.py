# Discord imports
import discord
from discord.ext import commands

# Built-in imports
from sys import path
import re

path.append('../')

# In-house imports
import main
from lib import botlogger as log
import lib.sheets as sheets

# Dependency imports
from psycopg2 import connect

logging_sheets = log.Logger(log.Configuration('sheets'))

class Sheets:
	def __init__(self, bot):
		self._bot = bot


	@commands.group(name="sheets", help='- Command group for sheets. !help sheets for commands.')
	async def sheets_group(self, ctx):
		"""- Command group for sheets. Do !help sheets for help!"""
		pass


	@sheets_group.command()
	async def create(self, ctx):
		"""- This is how you register!"""

		logging_sheets.send(f'User {ctx.message.author.name} has started the registration process')

		if ctx.message.author.dm_channel is None:
			await ctx.message.author.create_dm()

		connection = connect('dbname=userdat.db user=postgres')
		cursor = connection.cursor()

		cursor.execute('SELECT 1 FROM playerdat WHERE USERID=%s;', (ctx.message.author.id,))

		if cursor.fetchone() == 1:
			await ctx.message.author.dm_channel.send('You are already in the system.')

		else:
			regex = re.compile('[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?')

			await ctx.message.author.dm_channel.send('Please enter your email address. By giving your email address, you agree to our Terms of Service and Privacy Policy. For our ToS and Privacy Policy, please type !tos. Keep in mind that for character creation, the email *must* be valid.')

			msg = ''

			while msg is not None:
				msg = await self._bot.wait_for('message', check=lambda msg: msg.author is ctx.message.author and not msg.content.startswith('!') and msg.channel is msg.author.dm_channel)
				match = regex.match(msg.content)

				if match is None:
					await ctx.message.author.dm_channel.send('Invalid email address')
					logging_sheets.verbose(f'Failed email registration attempt by {msg.author.name} with email {msg.content}')

				else:
					email = match.group()

					cursor.execute('SELECT 1 FROM playerdat WHERE EMAIL=%s;', (email,))

					if cursor.fetchone() == 1:
						await ctx.message.author.dm_channel.send(f'Email {email} is already taken.')
						logging_sheets.verbose(f'Failed email registration attempt by {msg.author.name} with registered email {email}')

					else:
						sheets.init_user(ctx.message.author.id, email)

						await ctx.message.author.dm_channel.send(f'Email address {email} has been accepted, and the character sheet has been created. Please check your Google Drive/Google Sheets.')
						logging_sheets.verbose(f'Successful registration attempt by {msg.author.name} with email {email}')
						break

			if msg is None:
				await ctx.message.author.dm_channel.send('Request has timed out. To restart your registration process, please type !sheets create.')
				logging_sheets.send(f'Registration attempt by {msg.author.name} has timed out.')

		cursor.close()
		connection.close()


	@commands.cooldown(1, 43200.0, commands.BucketType.member)
	@sheets_group.command()
	async def reset(self, ctx):
		"""- Deletes your character sheet and creates a new one, fully fixed and updated. Has a 12-hour cooldown"""
		sheets.update_spreadsheet(ctx.message.author.id)


	@commands.cooldown(1, 86400.0, commands.BucketType.member)
	@sheets_group.command(name='update email')
	async def email(self, ctx):
		"""- Allows you to update your email address"""

		if ctx.message.author.dm_channel is None:
			await ctx.message.author.create_dm()

		connection = connect('dbname=userdat.db user=postgres')
		cursor = connnection.cursor()

		regex = re.compile('[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?')

		await ctx.message.author.dm_channel.send('Please enter your mew email address. By giving your email address, you agree to our Terms of Service and Privacy Policy. For our ToS and Privacy Policy, please type !tos. Keep in mind that the email *must* be valid.')

		msg = ''

		while msg != None:
			msg = await self._bot.wait_for('message', check=lambda msg: msg.author is ctx.message.author and not msg.content.startswith('!') and msg.channel is msg.author.dm_channel)
			match = regex.match(msg.content)

			if match is None:
				await ctx.message.author.dm_channel.send('Invalid email address')
				logging_sheets.verbose(f'Failed email update attempt by {msg.author.name} with test email {msg.content}')

			else:
				email = match.group()[0]

				cursor.execute('SELECT 1 FROM playerdat WHERE EMAIL=%s', (email,))

				if cursor.fetchone() == 1:
					await ctx.message.author.dm_channel.send(f'Email {email} is already taken.')
					logging_sheets.verbose(f'Failed email update attempt by {msg.author.name} with already registered email {email}')

				else:
					sheets.init_user(ctx.message.author.id, email)

					await ctx.message.author.dm_channel.send(f'Email address {email} has been accepted, and the character sheet has been created. Please check your Google Drive/Google Sheets.')
					logging_sheets.verbose(f'Successful email update attempt by {msg.author.name} with new email {email}')
					break

		if msg is None:
			await ctx.message.author.dm_channel.send('Request has timed out. To restart your update process, please wait 1 day and type !sheets update email.')
			logging_sheets.send(f'Email update attempt by {msg.author.name} has timed out.')

		cursor.close()
		connection.close()


def setup(bot):
	bot.add_cog(Sheets(bot))