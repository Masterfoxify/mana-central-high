# Discord imports
import discord
from discord.ext import commands

# Built-in imports
from sys import path
from datetime import datetime
from json import dumps

path.append('../')

# In-house imports
import main
from lib import botlogger as log
from lib.players import Player
from lib.diceroller import auto_roll as roll
from lib.combat import Combat
from lib.grid import Grid as Grid_

logging_grid = log.Logger(log.Configuration('grid'))

class Grid:
	def __init__(self, bot):
		self._bot = bot


	@commands.group(name="init", help='- Command group for initiative. !help init for commands.')
	@commands.check(main.rp_channel_check)
	async def init_group(self, ctx):
		pass


	@init_group.command()
	@commands.check(main.rp_channel_check)
	async def join(self, ctx, x: int, y: int):
		"""- Joins the current initiative in the channel"""

		local_cache = main.get_cache()

		if ctx.message.channel.id not in local_cache.keys():
			await ctx.send('There is not an active combat in this channel.')

			logging_grid.verbose(f'Failed init `join` call by {message.author.name} in {ctx.message.channel.name}.')

		else:
			logging_grid.verbose(f'Successful init `join` call by {message.author.name} in {ctx.message.channel.name}.')

			local_cache[ctx.message.channel.id].players.append(Player.db_load_from_id(ctx.author.id,
														roll(1, 20, add = Player.db_get_init(ctx.author.id))))

			local_cache[ctx.message.channel.id].players.sort(key=lambda player: player.get_initiative())

			main.update_cache(local_cache)

	@init_group.command()
	@commands.check(main.rp_channel_check)
	async def create(self, ctx, x: int, y: int, height: int = 25, width: int = 25):
		"""- Creates an initiative in the channel"""

		local_cache = main.get_cache()

		if ctx.message.channel.id in local_cache.keys():
			await ctx.send(f'{ctx.author.mention} Initiative already exists. Did you mean `!init join`?')

			logging_grid.verbose(f'Failed init `create` call by {message.author.name} in {ctx.message.channel.name}.')

		else:
			placement = (x and y) and "" or " Please use !grid move x, y or be placed randomly."
			await ctx.send('Created initiative.' + placement)

			local_cache[ctx.message.channel.id] = Combat(Grid_(Player.db_load_from_id(ctx.message.author.id)), ctx.message.channel, height=height, width=width)


	@commands.group(name="grid", help='- Command group for grid movement and creation. !help grid for commands.')
	@commands.check(main.rp_channel_check)
	async def grid(self, ctx):
		pass


	@grid_group.command()
	@commands.check(main.rp_channel_check)
	async def get(self, ctx):
		"""- Gets the grid image of the channel you're in"""

		local_cache = main.get_cache()

		if ctx.message.channel.id not in local_cache.keys():
			await ctx.send('There is not an active combat in this channel.')

			logging_grid.verbose(f'Failed grid `get` call by {message.author.name} in {ctx.message.channel.name}.')

		else:
			grid_Object = local_cache[ctx.message.channel.id]

			image = grid_Object.render_table()

			logging_grid.verbose(f'Successful grid `get` call by {message.author.name} in {ctx.message.channel.name}')
			await ctx.send(file=image)

		await ctx.message.delete()


def setup(bot):
	bot.add_cog(Grid(bot))