# Discord imports
import discord
from discord.ext import commands
from discord.errors import Forbidden
from discord.ext.commands.errors import MissingRequiredArgument

# Built-in imports
from datetime import datetime
from os import makedirs, listdir, path
from random import choice
from json import loads, dumps
from json import JSONDecodeError
import traceback

# Important for use of gogbot
import builtins

# Dependency imports
from psycopg2 import connect

# In-house imports
import config as conf
from lib import botlogger as log


"""
	Global variables
"""


desc_file = 'bot_description.txt'

try:
	with open(desc_file) as f:
		description = f.read()

except FileNotFoundError:
	with open('../' + desc_file) as f:
		description = f.read()

bot = commands.Bot(command_prefix='!', description=description, pm_help=True)
eb_bot = commands.Bot(command_prefix='!', description=description, pm_help=True)

reaction_cache = {}

logger_file = conf.LOGFILE_STYLE()

logger = log.Logger(log.Configuration('main'))
file_logger = log.Logger(config=log.Configuration('main', level=log.ERROR, destination=logger_file, rotating=True))


"""
	Game Globals
"""


active_combats = {}


"""
	Event functions
"""


@bot.event
async def on_ready():
	"""Function called when the bot finishes its connection and successfully logs in.
	"""

	global reaction_cache

	date = datetime.now()

	logger.send(f'Logged in as: {bot.user.name}, {bot.user.id}')
	logger.send('-' * 64)
	logger.send(f'Date: {date.month}/{date.day}/{date.year} at {date.hour}:{date.minute}:{date.second}\n')

	try:
		with open('reaction_messages.cache', 'r') as f:
			reaction_cache = dict(loads(f.read()))
			logger.send(f'reaction_cache on bootup: {reaction_cache}\n')

	except (TypeError, JSONDecodeError):
		reaction_cache = {}

		with open('reaction_messages.cache', 'w') as f:
			f.write(dumps(reaction_cache))

	extension_load()


def extension_load():
	"""Loads all of the extensions. Was originally in the __name__ block, but moved for proper global variable importation.
	"""

	extensions = listdir('cogs')

	# Parenthesis added purely for readability.
	# Checks to make sure it doesn't load any potentially generated folders/files.
	extension_check = lambda ext: ('__' not in ext) and (ext.replace('.py', '')) or None
	startup_extensions = [extension_check(extension) for extension in extensions]

	for extension in startup_extensions:

		try:
			if not extension is None:
				bot.load_extension('cogs.' + extension)
				logger.send(f'Loaded extension {extension}...')

		except ImportError as e:
			exc = f'{type(e).__name__}: {e}'
			logger.send(f'Failed to load extension {extension}\n{exc}\n')

	logger.send('Bot startup extension loading has completed. Bot is now ready for interaction.')
	logger.send('-' * 64 + '\n')


@bot.event
async def on_message(message: discord.Message):
	"""Function called on all messages. Used for ranking system and command calling.
	"""
	
	if not message.author.bot:
		cursor.execute('SELECT 1 FROM userdat WHERE USERID=%s;', (message.author.id,))

		if cursor.fetchone() is None:
			cursor.execute('INSERT INTO userdat VALUES (%s,%s,%s);', (message.author.id, 1, ''))
			connection.commit()
			exp = 1

		else:
			cursor.execute('SELECT RANKEXP FROM userdat WHERE USERID=%s LIMIT 1;', (message.author.id,))
			exp = cursor.fetchone()[0]

			cursor.execute('UPDATE userdat SET RANKEXP=%s WHERE USERID=%s;', (int(exp) + 1, message.author.id))
			connection.commit()

		logger.verbose(f'{message.author.name} has {int(exp)} experience, and is level {conf.level_formula(exp)}')

		try:
			await bot.process_commands(message)


		except MissingRequiredArgument:
			await message.channel.send('**Error:** Missing required argument. Please check your syntax.')

'''
@bot.event
async def on_command_error(ctx, error):
	"""Catches all errors from commands.
	"""

	if isinstance(error, (MissingRequiredArgument,)):
		await ctx.send('**Error:** Missing required argument. Please check your syntax.')

	file_logger.error(error.original)
	logger.error(str(error).split('\n')[-1])'''


@bot.event
async def on_member_join(member: discord.Member):
	"""Adds the user into the database.
	"""

	cursor.execute('SELECT 1 FROM userdat WHERE USERID=%s;', (member.id,))

	if not bool(cursor.fetchone()):
		cursor.execute('INSERT INTO userdat VALUES (%s,%s,%s);', member.id, 0, '')
		connection.commit()


@bot.event
async def on_raw_reaction_add(payload: discord.RawReactionActionEvent):
	"""If reaction is cached, it will give the user the linked role.
	"""

	global reaction_cache
	await get_cache()

	emoji = str(payload.emoji)
	guild = bot.get_guild(payload.guild_id)
	member = guild.get_member(payload.user_id)
	message_id = str(payload.message_id)

	
	if message_id in reaction_cache.keys():

		if str(emoji) in reaction_cache[message_id].keys():

			role = discord.utils.get(guild.roles,
				name=reaction_cache[message_id][str(emoji)])

			try:
				await member.add_roles(role)
				logger.send(f'Added role {role} to {member.name} successfully.')

			except Forbidden:
				logger.error(f'Tried to add role {role} to {member.name} but did not have sufficient permission.')

			except Exception as e:
				divider = '-' * 30
				logger.error(f'Tried to add role {role} from {member.name} but an unknown error occured. ' +
					'Unknown exception has been logged in logs folder.\n')
				file_logger.error(f'Tried to add role {role} from {member.name} but an unknown error occured.\n{divider}\n{e}\n{divider}\n' + 
					'Please contact Masterfoxify#8170 (user id 121097842071699456) to add special exception handling.\n')

				date = datetime.now()

				file_logger.error(e)


@bot.event
async def on_raw_reaction_remove(payload: discord.RawReactionActionEvent):
	"""If reaction is cached, it will remove the user's linked role.
	"""

	global reaction_cache
	await get_cache()

	emoji = str(payload.emoji)
	guild = bot.get_guild(payload.guild_id)
	member = guild.get_member(payload.user_id)
	message_id = str(payload.message_id)

	if message_id in reaction_cache.keys():

		if str(emoji) in reaction_cache[message_id].keys():

			role = discord.utils.get(guild.roles,
				name=reaction_cache[message_id][str(emoji)])

			try:
				await member.remove_roles(role)
				logger.send(f'Removed role {role} from {member.name} successfully.')

			except Forbidden:
				logger.error(f'Tried to remove role {role} from {member.name} but did not have sufficient permission.')

			except Exception as e:
				divider = '-' * 30
				logger.error(f'Tried to remove role {role} from {member.name} but an unknown error occured.\n{divider}\n{e}\n{divider}\n' + 
					'Please contact Masterfoxify#8170 (user id 121097842071699456) to add special exception handling.\n' +
					'Unknown exception has been logged in logs folder.\n')

				date = datetime.now()

				if not path.isdir('/logs'):
					makedirs('/logs')

				with open(f'logs/{date.month}-{date.day}-{date.year}-{date.hour}-{date.minute}-{date.second}', 'w+') as f:
					f.write(e)


@bot.command()
async def tos(ctx):
	"""- Sends the official Terms of Service"""

	with open('./legal/Mana Central High ToS.docx', 'rb') as f:
		await ctx.send('', file=discord.File(f, filename='Mana Central High Terms of Service.docx'))


@bot.command()
@commands.has_role("Admin")
async def load(ctx):
	connection = connect('dbname=mch_userdat user=postgres')
	cursor = connection.cursor()

	cursor.execute('SELECT * FROM userdat;')

	x = 1

	while x is not None:
		x = cursor.fetchone()
		print(x)


"""
	Cog functions
"""


def rp_channel_check(ctx):
	"""Checks to see if the channel is in an RP category."""

	mch_categories = [x for x in discord.utils.get(bot.guilds, name='Mana Central High').categories if x.id in conf.RP_CATEGORIES]
	
	for category in conf.RP_CATEGORIES:
		if ctx.message.channel in category.channels:
			return True

	return False


def load(extension_name: str, extension_dir: str = ''):
	"""Loads a cog extension into the bot."""
	try:
		bot.load_extension(extension_dir.replace('/', '.') + extension_dir and '.' or '' + extension_name)

	except (AttributeError, ImportError) as e:
		date = datetime.now()

		if not path.isdir('/logs'):
			makedirs('/logs')

		with open(f'logs/{date.month}-{date.day}-{date.year}-{date.hour}-{date.minute}-{date.second}', 'w+') as f:
			f.write(e)

	logger.debug('{} loaded successfully.'.format(extension_dir.replace('/', '.') + extension_dir and '.' or '' + extension_name))


def unload(extension_name: str, extension_dir: str = ''):
	"""Unloads a cog extension from the bot."""
	bot.unload_extension(extension_dir.replace('/', '.') + extension_dir and '.' or '' + extension_name)
	logger.debug('{} loaded successfully.'.format(extension_dir.replace('/', '.') + extension_dir and '.' or '' + extension_name))


'''------------------------------------------------------------------------------------------------------------------------------------------------------------------------------'''

async def get_cache():
	"""Used to get cache.
	"""

	if __name__ == '__main__':
		global reaction_cache

		with open('reaction_messages.cache', 'r') as f:
			try:
				reaction_cache = dict(loads(f.read()))
				logger.send(f'Cache pulled: {reaction_cache}')

			except JSONDecodeError:
				reaction_cache = {}
				logger.error('Error while loading cache. Cache has been reset.')

			except ValueError:
				pass

	else:
		with open('reaction_messages.cache', 'r') as f:
			return dict(loads(f.read()))


async def update_cache(cache=reaction_cache):
	"""Notifies bot console of cache updates and saves them accordingly.
	"""

	with open('reaction_messages.cache', 'w') as f:
		f.write(dumps(cache))

	logger.send(f'Cache updated: {cache}')


async def get_combats():
	"""Used to get combat cache
	"""

	if __name__ == '__main__':
		global active_combats

		with open('combats.cache', 'r') as f:
			try:
				active_combats = dict(loads(f.read()))
				logger.send(f'Cache pulled: {active_combats}')

			except JSONDecodeError:
				active_combats = {}
				logger.error('Error while loading cache. Cache has been reset.')

			except ValueError:
				pass

	else:
		with open('combats.cache', 'r') as f:
			return dict(loads(f.read()))


async def update_combats(cache=reaction_cache):
	"""Notifies bot console of combat cache updates and saves them accordingly.
	"""

	with open('combats.cache', 'w') as f:
		f.write(dumps(cache))

	logger.send(f'Cache updated: {cache}')


if __name__ == '__main__':

	global connection
	global cursor

	# Checking to see if the databases have been properly created.
	first_time_setup_check = listdir('./')
	if 'mch_userdat' not in first_time_setup_check:

		while True:
			do_setup = input('Databases not found. Begin first time setup? Y/N: ')

			if do_setup.upper() == 'Y':
				conn = connect('dbname=postgres')
				conn.autocommit = True
				cursor = conn.cursor()
				cursor.execute('CREATE DATABASE userdat;')
				print('Creating database file userdat...')
				cursor.close()
				conn.close()

				conn = connect('dbname=userdat user=postgres')

				print('Creating connection to database file userdat...')
				connection = connect('dbname=mch_userdat user=postgres')
				cursor = connection.cursor()

				print('Creating table userdat in database file userdat...')
				cursor.execute('''CREATE TABLE userdat
								(USERID real, RANKEXP real, WARNINGS text);''')

				connection.commit()

				print('Creating table playerdat in database file userdat...')
				cursor.execute('''CREATE TABLE playerdat
								(

								USERID real, NAME text, EXP real, RANK text,
									AGE real, BIRTHDAY text, HEIGHT text, YEAR real, GENDER text, AURA text,
									IMAGE1 text, IMAGE2 text, THEME text, INFO text, BACKSTORY text,

								PROTECTOR real, MAGE real, WARRIOR real, CLERIC real, HERO real,

								TITAN real, PALADIN real, BERSERK real, WIZARD real, SAGACIOUS real, SORCERER real, KNIGHT real,
									DEFENDER real, MERCENARY real, PRIEST real, ASSASSIN real, BANDIT real,

								STR real, DEX real, DUR real, INTEL real, CHA real, WIS real, PROT real, MANA real,

								CURRENTAURA real, CURRENTHEALTH real, CURRENTMANA real,
								ABILITIES text

								);''')

				cursor.execute('''CREATE TABLE monsterdat
								(

								MONSTERID real, NAME text NOT NULL, THEME text,

								PROTECTOR real, MAGE real, WARRIOR real, CLERIC real, HERO real,

								TITAN real, PALADIN real, BERSERK real, WIZARD real, SAGACIOUS real, SORCERER real, KNIGHT real,
									DEFENDER real, MERCENARY real, PRIEST real, ASSASSIN real, BANDIT real,

								STR real, DEX real, DUR real, INTEL real, CHA real, WIS real, PROT real, MANA real,

								CURRENTAURA real, CURRENTHEALTH real, CURRENTMANA real,
								ABILITIES text, AI real

								);''')

				connection.commit()

				connection.close()

				print('First-time setup is now complete.\n' + ('-' * 64) + '\n')
				break

			elif do_setup.upper() == 'N':
				break

	else:
		connection = connect('dbname=mch_userdat user=postgres')
		cursor = connection.cursor()

	with open('token.txt') as f:
		bot.run(f.readlines()[0])
		eb_bot.run(f.readlines()[1])