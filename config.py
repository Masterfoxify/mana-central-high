import discord
from datetime import datetime
import math

"""
	Constants to be left UNTOUCHED
"""

DEBUG = 10 # For debugging purposes
DEFAULT = 20 # The default, essentially 'INFO' on normal logger
ERROR = 30 # For explicitly ONLY error messages

CONSOLE = 1

"""
	Logger config
"""

verbose = True

DEFAULT_LOGGER = {
		'name': 'root',
		'level': DEBUG, 
		'destination': CONSOLE,
		'write_type': 'a',
		'rotating': False,
		'message_style': None
}

LOGFILE_STYLE = lambda: f'./logs/{datetime.now().month}_{datetime.now().day}_{datetime.now().year}-{datetime.now().hour}_{datetime.now().minute}.log'

# Alternate file default for DEFAULT_LOGGER

#DEFAULT_LOGGER =
#{
#		name: 'root',
#		level = DEFAULT, 
#		destination = '../logs/log.log',
#		write_type = 'a',
#		rotating = True,
#		message_style = None
#}


"""
	Level formula
"""


level_formula = lambda exp: int(math.floor(exp ** (1 / 4) * exp ** (1 / 3) + 1))
to_next_level = lambda exp: int(math.ceil(level_formula(exp) ** (12 / 7) - exp))


"""
	Embed configs
"""


level_embed = lambda exp: discord.Embed(colour=discord.Colour(16777215 // int(exp)),
	description=f'\n```CSS\nLevel: {level_formula(exp)}```')

# What should follow:
#embed.set_thumbnail(url="URL")
#embed.set_author(name="user's name")

# To send embed:
#await bot.say(embed=embed)

"""
	Sheets configs
"""

MAIN_SHEET = '1i49zIfrNODLaGRVFDNz2Ad24NZBjPnK2dkVbta4nvW8'


"""
	Server configs
"""


RP_CATEGORIES = []


#######################################
### Character Description Worksheet ###
#######################################

FULL_NAME = 'F5'
RANK = 'F5'

# BASIC INFO

AGE = 'F7'
BIRTHDAY = 'F8'
HEIGHT = 'F9'
YEAR = 'F10'
GENDER = 'F7'
EXP = 'F9'
LEVEL = 'F10'

AURA = 'C12'
IMAGE1 = 'F12'
IMAGE2 = 'E12'
THEME = 'F12'

INFO = 'C14'
BACKSTORY = 'C23'


############################
### Statistics Worksheet ###
############################

# STATS

STR = 'F17'
DEX = 'F18'
DUR = 'F19'
MANA = 'F20'

INTEL = 'F17'
CHA = 'F18'
WIS = 'F19'
PROT = 'F20'

# CLASSES

PROTECTOR = 'M6'
MAGE = 'M7'
WARRIOR = 'M8'
CLERIC = 'M9'
HERO = 'M10'

# SUBCLASSES

TITAN = 'O6'
PALADIN = 'O7'
BERSERK = 'O8'
WIZARD = 'O9'
SAGACIOUS = 'O10'
SORCERER = 'O11'
KNIGHT = 'O12'
DEFENDER = 'O13'
MERCENARY = 'O14'
PRIEST = 'O15'
ASSASSIN = 'O16'
BANDIT = 'O17'

# RESOURCES

DAMAGE = 'M19'
MANA_USED = 'M22'
TRUE_DMG = 'M25'

# FORMULAS

skill_min = lambda level: int(10 + 4.75 * level - 4.75)
skill_max = lambda level: 60 + level * 5 - 5

###############################
### Buffs/Debuffs Worksheet ###
###############################

# STATIC

STATIC_AURA = 'D7'
STATIC_ARMOR = 'D8'
STATIC_SHIELD = 'D9'
STATIC_PHY = 'D10'
STATIC_MAG = 'D11'
STATIC_RESERVE = 'D12'
STATIC_DETECTION = 'D13'
STATIC_CONTROL = 'D14'
STATIC_RECOVERY = 'D15'
STATIC_INVEST = 'D16'
STATIC_STUDY_BODY = 'D17'
STATIC_STUDY_TRANSFORMATION = 'D18'
STATIC_STUDY_AURA = 'D19'
STATIC_STUDY_CREATION = 'D20'
STATIC_STUDY_DESTRUCTION = 'D21'
STATIC_STUDY_LIFE = 'D22'

# PERCENT

PERCENT_AURA = 'F7'
PERCENT_ARMOR = 'F8'
PERCENT_SHIELD = 'F9'
PERCENT_PHY = 'F10'
PERCENT_MAG = 'F11'
PERCENT_RESERVE = 'F12'
PERCENT_DETECTION = 'F13'
PERCENT_CONTROL = 'F14'
PERCENT_RECOVERY = 'F15'
PERCENT_INVEST = 'F16'
PERCENT_STUDY_BODY = 'F17'
PERCENT_STUDY_TRANSFORMATION = 'F18'
PERCENT_STUDY_AURA = 'F19'
PERCENT_STUDY_CREATION = 'F20'
PERCENT_STUDY_DESTRUCTION = 'F21'
PERCENT_STUDY_LIFE = 'F22'