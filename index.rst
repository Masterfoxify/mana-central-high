.. Mana Central High documentation master file, created by
   sphinx-quickstart on Thu Mar 14 13:39:01 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Mana Central High's documentation!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
